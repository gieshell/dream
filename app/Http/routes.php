<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('404',function(){
	abort(404);
});

Route::group(['middleware'=>'login'],function(){
	//后台首页
	Route::Controller('/admin/','AdminController');
	//后台用户
	Route::Controller('/admin/user','UserController');
	//退出登录
	Route::get('/admin/login/loginout','LoginController@loginout');
});


//后台登录
Route::Controller('/admin/login','LoginController');

//前台用户首页
Route::Controller('/home','HomeController');
Route::Controller('/home/login','LoginController');