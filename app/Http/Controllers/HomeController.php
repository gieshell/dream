<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * 这个是后台的首页
     * /admin/index
     */
    public function getIndex()
    {
    	return view('home.index');
    }
    
}
