<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class LoginController extends Controller
{
    /**
     * 这个是后台登录
     * /admin/login/index
     */
    public function getIndex()
    {
    	return view('admin.login.login');
    }

    /**
     * 这个是登录处理页面(前后台)
     * /admin/login/Check
     * /admin/home/login
     */
    public function postCheck(Request $request)
    {
    	//查询用户是否存在
    	$user = DB::table('ym_user')->where('username',$request->input('username'))->first();
        //判断 用户名是否存在
        if($user){
            //存在的时候并且登录 admin
                 if($user->username == 'admin'){
                    if(Hash::check($request->input('password'),$user->password)){
                        //登录成功 用户id放入session中
                        session(['id'=>$user->id,'login'=>'1']);
                        return redirect('/admin')->with('success','欢迎您'.$user->username.'登录')->with('username',$user->username);
                    }else{
                        return back()->with('error','用户名或密码错误');
                    }
                }else{
            //不是admin的时候跳转前台
                    if(Hash::check($request->input('password'),$user->password)){
                        //登录成功 用户id放入session中
                        session(['id'=>$user->id]);
                        return redirect('/home/index');
                    }else{
                        return back()->with('error','用户名或密码错误');
                    }
                }
        }else{
             return back()->with('error','用户名或密码错误');
                
        }



    }

    public function loginout(Request $request)
    {
        //用户退出登录
        $request->session()->flush();
        return redirect('admin/login');
    }
    
}
