<?php

namespace App\Http\Controllers;
use Hash;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\InsertArticleRequest;
class UserController extends Controller
{

	/**
	 *  这个是用户的列表首页
	 *  /admin/user/index
	 */
	public 	function getIndex(Request $request)
	{	

		//读取数据并且分页显示
		//$date = DB::table('ym_user')->paginate($request->input('num',7));
		$date = DB::table('ym_user')->where(function($query)use($request){
			$query->where('username','like','%'.$request->input('seek').'%');
		})->paginate($request->input('num',7));
		return view('admin.user.index',['date'=>$date,'request'=>$request->all()]);
	}

	/**
	 * 这个是后台的添加页面
	 * /admin/user/add
	 */
	public function getAdd()
	{	
		//返回模板
		return view('admin.user.add');
	}

    /**
     * 这个是给后台添加用户的ajax使用的
     * /admin/user/select
     */
    public function getSelect()
    {
    	$date = $_GET['username'];
    	$username = DB::select('select * from ym_user where username="'.$date.'"');
    	if($username){
    		//1 是用户名存在
    		echo 1;
    	}else{
    		//2 是用户名不存在
    		echo 2;
    	}

    }



    /**
     *  这个是接受用户添加的form表单
     * 	/admin/user/insert
     *  InsertArticleRequest 这个是用户提交过来的数据进行验证
     */
    public function postInsert(InsertArticleRequest $request)
    {	

    	//接受数据 
    	$date = $request->except(['repassword','_token']);
    	//加密密码
    	$date['password'] = Hash::make($request->input('password'));
    	//对token字段进行处理随机生成50个字符串
    	$date['token'] = str_random(50);
    	$date['regtime'] = time();
    	//将数据插入数据库中
    	$res = DB::table('ym_user')->insert($date);
    	if($res){
    		return redirect('admin/user/index')->with('success','用户添加成功');
    	}else{
    		return redirect('admin/user/add');
    	}
    }

    /**
     * 这个是个用户的修改操作
     * /admin/user/edit
     */
    public function getEdit($id)
    {
    	//查询数据
    	$res = DB::table('ym_user')->where('id',$id)->first();
    	//显示模板
    	return view('admin.user.edit',['userinfo'=>$res]);
    }

    /**
     * 这个是用户的修改界面
     * /admin/user/update
     */
    public function postUpdate(Request $request)
    {	
    	$date = $request->except(['_token']);
    	$res = DB::table('ym_user')->where('id',$request->id)->update($date);
    	if($res >= 0){
    		return redirect('admin/user/index')->with('success','用户信息修改成功');
    	}else{
    		return redirect('admin/user/edit');
    	}
    }

    /**
     * 这个是用户的修改界面
     * /admin/user/delt
     */
    public function getDelt()
    {	$id = $_GET['id'];
    	$res = DB::table('ym_user')->where('id',$id)->delete();
    	if($res){
    		//1 删除成功
    		echo 1;
    	}else{
    		//2 删除失败
    		echo 2;
    	}
    }
}
