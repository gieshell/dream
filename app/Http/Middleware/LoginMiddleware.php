<?php

namespace App\Http\Middleware;

use Closure;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //检测用户是否登录
        $data = $request->session()->all();
        if($data = $request->session()->has('login')){
           /* if($data['login'] == 1){*/
                return $next($request);
          /*  }else{
                return redirect('/admin/login/index')->with('error','你还没有登录');
            }*/
        }else{
            return redirect('/admin/login/index')->with('error','你还没有登录');
        }

        
    }
}
