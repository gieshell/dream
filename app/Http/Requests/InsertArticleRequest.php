<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InsertArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  判断是否满足验证规则。
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required',
            'repassword' => 'required|same:password',
            'email' => 'required|email'
        ];
    }

    /**
     * 获取已定义验证规则的错误消息。
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => '标题是必填的',
            'password.required'  => '密码是必填的',
            'repassword'.'required' => '重置密码不能为空',
            'repassword'.'same' => '两次密码不一致',
            'email'.'required' => '用户邮箱不能为空',
            'email'.'email' => '用户邮箱不符合规则',

        ];
    }
}
