@extends('layout.index');
@section('title', '用户添加')
@section('content')
      <div class="row-fluid">
		<div class="span12">

			<div id="form_wizard_1" class="portlet box blue">

				<div class="portlet-title">

					<div class="caption">

						<i class="icon-reorder"></i><span class="step-title">修改用户信息</span>
						
					</div>

					<div class="tools hidden-phone">

						<a class="collapse" href="javascript:;"></a>

						<a class="config" data-toggle="modal" href="#portlet-config"></a>

						<a class="reload" href="javascript:;"></a>

						<a class="remove" href="javascript:;"></a>

					</div>

				</div>

				<div class="portlet-body form">

					<form id="submit_form" class="form-horizontal"  method="POST" action="/admin/user/update" novalidate="novalidate">
						{{ csrf_field() }}
						<div class="form-wizard">

							<div class="navbar steps">

								<div class="navbar-inner">

									<ul class="row-fluid nav nav-pills">

										<li class="span7 jd1">

											<a class="step" data-toggle="tab" href="#tab2">

											<span class="number">2</span>

											<span class="desc"><i class="icon-ok"></i>修改用户金额</span>   

											</a>

										</li>

										<li class="span4 jd2">

											<a class="step" data-toggle="tab" href="#tab3">

											<span class="number">3</span>

											<span class="desc"><i class="icon-ok"></i>修改用户邮箱</span>   

											</a>

										</li>


									</ul>

								</div>

							</div>

							<div class="progress progress-success progress-striped" id="bar">

								<div id="jdt" class="bar" style="width: 0%;"></div>

							</div>

							<div class="tab-content">

								<div class="alert alert-error hide">

									<button data-dismiss="alert" class="close"></button>

									You have some form errors. Please check below.

								</div>

								<div class="alert alert-success hide">

									<button data-dismiss="alert" class="close"></button>

									Your form validation is successful!

								</div>

								<div id="tab1" class="tab-pane active">

									<h3 class="block">修改用户信息</h3>
										<!-- 验证操作 -->

										<div id="useryz" class="">
										<p>


										</p>

									</div>

									<div class="control-group">

										<label class="control-label">用户名<span class="required">*</span></label>

										<div class="controls">

											<input type="text" name="username" value="{{$userinfo->username}}" disabled="" class="span6 m-wrap"><span data-original-title="please write a valid email" class=""><i class=""></i></span>
											<input type="hidden" name="id" value="{{$userinfo->id}}"  class="span6 m-wrap"><span data-original-title="please write a valid email" class=""><i class=""></i></span>

										</div>

									</div>
									<div class="control-group">

										<label class="control-label">用户金额<span class="required">*</span></label>

										<div class="controls">

											<input type="text" name="money" value="{{$userinfo->money}}"  class="span6 m-wrap"><span data-original-title="please write a valid email" class=""><i class=""></i></span>
										</div>

									</div>

									<div class="control-group">

										<label class="control-label">邮箱<span class="required">*</span></label>

										<div class="controls">

											<input type="text" value="{{$userinfo->email}}"name="email" class="span6 m-wrap">

											

										</div>

									</div>

								</div>



								
							<div class="form-actions clearfix">

								<a class="btn button-previous" href="javascript:;" style="display: none;">

								<i class="m-icon-swapleft"></i> Back 

								</a>

								<button class="btn blue">修改信息</button>

								<i class="m-icon-swapright m-icon-white"></i>

								</a>

								<a class="btn green button-submit" href="javascript:;" style="display: none;">

								Submit <i class="m-icon-swapright m-icon-white"></i>

								</a>

							</div>

						</div>

					</form>

				</div>

			</div>

		</div>

</div>
<script>
	//声明全局变量 检测input 是否输入正确
		var MONEY = false;
		var EMAIL = false;

		//绑定表单提交事件
		$('form').submit(function(){
			//触发所有的丧失焦点事件
			$('input').trigger('blur');
			if( MONEY && EMAIL){
				return true;
			}

			//阻止默认行为
			return false;
		})

		//用户名丧失焦点事件
		//先不实用
/*		$('input[name=username]').blur(function(){
			//检测用户名是否正确
			var reg = /^\w{5,18}$/;
			//获取用户名
			var username = $(this).val();
			if(reg.test(username)){
				$(this).next().children().attr('class','icon-ok');
				$(this).next().attr('class','input-success tooltips');
				$(this).parent('div').addClass('controls input-icon');
				$(this).parents('.control-group').attr('class','control-group success');
				$('#jdt').css('width','30%');
				$('.jd').addClass('active');
				USER = true;
			}else{
				//错误
				$(this).next().children().attr('class','icon-exclamation-sign');
				$(this).next().attr('class','input-error error');
				$(this).parent('div').addClass('controls input-icon');
				$(this).parents('.control-group').attr('class','control-group error');
				return false;
			}
		})*/

		$('input[name=money]').blur(function(){
			//获取金钱的值
			var pass = $(this).val();
			//正则
			var reg = /^[0-9]+\.?[0-9]{0,2}$/;
			if(reg.test(pass)){
				//正确
				$(this).next().children().attr('class','icon-ok');
				$(this).next().attr('class','input-success tooltips');
				$(this).parent('div').addClass('controls input-icon');
				$(this).parents('.control-group').attr('class','control-group success');
				$('#useryz').attr('class','alert alert-block alert-success fade in').children().html('金额可以使用');
				$('#jdt').css('width','50%');
				$('.jd1').addClass('active');
				MONEY = true;
			}else{
				//错误
				$(this).next().children().attr('class','icon-exclamation-sign');
				$(this).next().attr('class','input-error error');
				$(this).parent('div').addClass('controls input-icon');
				$(this).parents('.control-group').attr('class','control-group error');
				$('#useryz').attr('class','alert alert-block alert-error fade in').children().html('金额不合法,小数点保留两位');
				return false;
			}
		})

		$('input[name=email]').blur(function(){
			//获取邮箱的值
			var email = $(this).val();
			//正则
			var reg = /^\w+@\w+\.(com|cn|com.cn|net|org)$/;
			//检测
			if(reg.test(email)){
				//正确
				$(this).next().children().attr('class','icon-ok');
				$(this).next().attr('class','input-success tooltips');
				$(this).parent('div').addClass('controls input-icon');
				$(this).parents('.control-group').attr('class','control-group success');
				$('#useryz').attr('class','alert alert-block alert-success fade in').children().html('邮箱格式正确');
				$('#jdt').css('width','100%');
				$('.jd2').addClass('active');
				EMAIL = true;
			}else{
				$(this).next().children().attr('class','icon-exclamation-sign');
				$(this).next().attr('class','input-error error');
				$(this).parent('div').addClass('controls input-icon');
				$(this).parents('.control-group').attr('class','control-group error');
				$('#useryz').attr('class','alert alert-block alert-error fade in').children().html('邮箱不合法');
				return false;
			}
		})
</script>
@endsection


