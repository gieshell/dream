@extends('layout.index');
@section('title', '用户添加')
@section('content')
      <div class="row-fluid">
		<div class="span12">

			<div id="form_wizard_1" class="portlet box blue">

				<div class="portlet-title">

					<div class="caption">

						<i class="icon-reorder"></i><span class="step-title">用户添加</span>
						
					</div>

					<div class="tools hidden-phone">

						<a class="collapse" href="javascript:;"></a>

						<a class="config" data-toggle="modal" href="#portlet-config"></a>

						<a class="reload" href="javascript:;"></a>

						<a class="remove" href="javascript:;"></a>

					</div>

				</div>

				<div class="portlet-body form">

					<form id="submit_form" class="form-horizontal"  method="POST" action="/admin/user/insert" novalidate="novalidate">
						{{ csrf_field() }}
						<div class="form-wizard">

							<div class="navbar steps">

								<div class="navbar-inner">

									<ul class="row-fluid nav nav-pills">
										<!-- jd 进度 -->
										<li  class="span3  jd">

											<a class="step active" data-toggle="tab" href="#tab1">

											<span class="number">1</span>

											<span class="desc"><i class="icon-ok"></i>用户名填写完成</span>   

											</a>

										</li>

										<li class="span3 jd1">

											<a class="step" data-toggle="tab" href="#tab2">

											<span class="number">2</span>

											<span class="desc"><i class="icon-ok"></i> 用密码填写完成</span>   

											</a>

										</li>

										<li class="span3 jd2">

											<a class="step" data-toggle="tab" href="#tab3">

											<span class="number">3</span>

											<span class="desc"><i class="icon-ok"></i>用户确认密码一致</span>   

											</a>

										</li>

										<li class="span3 jd3">

											<a class="step" data-toggle="tab" href="#tab4">

											<span class="number">4</span>

											<span class="desc"><i class="icon-ok"></i>用邮箱填写完成</span>   

											</a> 

										</li>

									</ul>

								</div>

							</div>

							<div class="progress progress-success progress-striped" id="bar">
								<!-- jdt 进度条 -->
								<div id="jdt" class="bar" style="width: 0%;"></div>

							</div>

							<div class="tab-content">

								<div class="alert alert-error hide">

									<button data-dismiss="alert" class="close"></button>

									You have some form errors. Please check below.

								</div>

								<div class="alert alert-success hide">

									<button data-dismiss="alert" class="close"></button>

									Your form validation is successful!

								</div>

								<div id="tab1" class="tab-pane active">

									<h3 class="block">用户添加 </h3>
										<!-- 验证操作 -->

										<div id="useryz" class="">
										<p>


										</p>

									</div>

									<div class="control-group">

										<label class="control-label">用户名<span class="required">*</span></label>

										<div class="controls">

											<input type="text" name="username"  placeholder="用户名6-18位可以是用字母数组下划线" class="span6 m-wrap"><span data-original-title="please write a valid email" class=""><i class=""></i></span>

											

										</div>

									</div>

									<div class="control-group">

										<label class="control-label">密码<span class="required">*</span></label>

										<div class="controls">

											<input type="password" id="submit_form_password" name="password" class="span6 m-wrap">

											

										</div>

									</div>

									<div class="control-group">

										<label class="control-label">确认密码<span class="required">*</span></label>

										<div class="controls">

											<input type="password" name="repassword" class="span6 m-wrap">

											

										</div>

									</div>

									<div class="control-group">

										<label class="control-label">邮箱<span class="required">*</span></label>

										<div class="controls">

											<input type="text" name="email" class="span6 m-wrap">

											

										</div>

									</div>

								</div>



								
							<div class="form-actions clearfix">

								<a class="btn button-previous" href="javascript:;" style="display: none;">

								<i class="m-icon-swapleft"></i> Back 

								</a>

								<button class="btn blue">添加账户</button>

								<i class="m-icon-swapright m-icon-white"></i>

								</a>

								<a class="btn green button-submit" href="javascript:;" style="display: none;">

								Submit <i class="m-icon-swapright m-icon-white"></i>

								</a>

							</div>

						</div>

					</form>

				</div>

			</div>

		</div>

</div>
	<script src="/adminPublic/media/js/repassword.js" type="text/javascript"></script>
@endsection


