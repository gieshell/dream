<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>驭梦商城会员登录</title>

	<meta content="width=device-width, initial-scale=1.0" name="viewport" />

	<meta content="" name="description" />

	<meta content="" name="author" />

	<!-- BEGIN GLOBAL MANDATORY STYLES -->

	<link href="/adminPublic/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/style-metro.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/style.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/default.css" rel="stylesheet" type="text/css" id="style_color"/>

	<link href="/adminPublic/media/css/uniform.default.css" rel="stylesheet" type="text/css"/>

	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL STYLES -->

	<link href="/adminPublic/media/css/login-soft.css" rel="stylesheet" type="text/css"/>

	<!-- END PAGE LEVEL STYLES -->

	<link rel="shortcut icon" href="/adminPublic/media/image/favicon.ico" />

</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="login">

	<!-- BEGIN LOGO -->

	<div class="logo">

		<img src="/adminPublic/media/image/logo-big.png" alt="" /> 

	</div>

	<!-- END LOGO -->

	<!-- BEGIN LOGIN -->

	<div class="content">

		<!-- BEGIN LOGIN FORM -->

		<form class="form-vertical login-form1" action="/admin/login/check" method="post">
			{{ csrf_field() }}
			<h3 class="form-title">驭梦商城会员登录</h3>
		@if(session('error'))
			<div class="alert alert-error">

				<button class="close" data-dismiss="alert"></button>

				<span>{{session('error')}}</span>

			</div>
		@endif
			<div class="control-group">

				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->

				<label class="control-label visible-ie8 visible-ie9">用户名</label>

				<div class="controls">

					<div class="input-icon left">

						<i class="icon-user"></i>

						<input class="m-wrap placeholder-no-fix" type="text" placeholder="用户名" name="username"/>

					</div>

				</div>

			</div>

			<div class="control-group">

				<label class="control-label visible-ie8 visible-ie9">密码</label>

				<div class="controls">

					<div class="input-icon left">

						<i class="icon-lock"></i>

						<input class="m-wrap placeholder-no-fix" type="password" placeholder="密码" name="password"/>

					</div>

				</div>

			</div>

			<div class="form-actions">       
				<input type="submit" value="登录" class="btn green big btn-block">
			</div>


		</form>

		<!-- END LOGIN FORM -->        



		<!-- END REGISTRATION FORM -->

	</div>

	<!-- END LOGIN -->

	<!-- BEGIN COPYRIGHT -->

	<div class="copyright">

		2016 &copy; 驭梦IT公司

	</div>

	<!-- END COPYRIGHT -->

	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

	<!-- BEGIN CORE PLUGINS -->

	<script src="/adminPublic/media/js/jquery-1.10.1.min.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>

	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

	<script src="/adminPublic/media/js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      

	<script src="/adminPublic/media/js/bootstrap.min.js" type="text/javascript"></script>

	<!--[if lt IE 9]>

	<script src="/adminPublic/media/js/excanvas.min.js"></script>

	<script src="/adminPublic/media/js/respond.min.js"></script>  

	<![endif]-->   

	<script src="/adminPublic/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/jquery.blockui.min.js" type="text/javascript"></script>  

	<script src="/adminPublic/media/js/jquery.cookie.min.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/jquery.uniform.min.js" type="text/javascript" ></script>

	<!-- END CORE PLUGINS -->

	<!-- BEGIN PAGE LEVEL PLUGINS -->

	<script src="/adminPublic/media/js/jquery.validate.min.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/jquery.backstretch.min.js" type="text/javascript"></script>

	<!-- END PAGE LEVEL PLUGINS -->

	<!-- BEGIN PAGE LEVEL SCRIPTS -->

	<script src="/adminPublic/media/js/app.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/login-soft.js" type="text/javascript"></script>      

	<!-- END PAGE LEVEL SCRIPTS --> 

	<script>

		jQuery(document).ready(function() {     

		  App.init();

		  Login.init();

		});

	</script>

	<!-- END JAVASCRIPTS -->

</body>

<!-- END BODY -->

</html>