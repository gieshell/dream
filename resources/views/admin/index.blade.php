@extends('layout.index')
@section('title', '后台首页')
@section('content')
<div class="dashboard">
      	<!-- success代表成功 -->
      	@if(session('success'))
      	<div class="alert alert-success">
      		
      		<button class="close" data-dismiss="alert"></button>

      		<strong>{{session('success')}}</strong>

      	</div>
      	@endif
</div>
@endsection
