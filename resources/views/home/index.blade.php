@extends('layout.home')
@section('title', '后台首页')
@section('content')
	<div class="hr-10"></div>
	<div class="g">
		<!--面包屑 -->
		<div class="breadcrumb-area fcn"><a href="/index.html" title="首页">首页</a>&nbsp;>&nbsp;<span id="bread-pro-name">HUAWEI P9 4GB+64GB 全网通版（金色）</span></div>
	</div>
	<div class="hr-10"></div>
	<div class="layout"> 
		<!--商品简介 -->
		<div class="pro-summary-area clearfix">
	    	<div class="right-area">
	        	<!--商品简介-属性 -->
	        	<div class="pro-property-area clearfix">
	                <div class="pro-meta-area">
	                	<h1 id="pro-name">HUAWEI P9 4GB+64GB 全网通版（金色）</h1>
	            	
	                	<!-- 插入 商品简介-广告语-->
	                	<div class="pro-slogan" id="skuPromWord">
	                	<!--
									精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！
								-->
	                			精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！
						</div>			
	                	<div class="hr-10"></div>
						<div class="line"></div>
	                
	                    <div class="pro-price">
								 <s id="pro-price-old"  class="hide"><label>华&nbsp;为&nbsp;&nbsp;价：</label>&yen;&nbsp; </s>
								 <span id="pro-price"><label>华&nbsp;为&nbsp;&nbsp;价：</label><b>&yen;&nbsp;3688.00</b></span>
	                    </div>
	                    
	                    <!--chenzhongxian 促销和赠品合并-->
	                    <!--商品简介-促销规则 -->
	                    <div id="pro-promotions-area" class="pro-promotions-area">
	                       <dl class="clearfix">
	                            <dt>优惠信息：</dt>
	                            <dd>
	                                <ul id="pro-promotions-list">
	                                
	                                </ul>
	                                <ul id="pro-gift-list" >
	                                    		<li><span>配&nbsp</span> <a href="/product/738132989.html#167810899" title="华为 HUAWEI Type C 转接头（白色）" target="_blank" >华为 HUAWEI Type C 转接头（白色）</a> </li>
	                                </ul> 
	                            </dd>
	                        </dl>
	                    </div>
	                    
	                    <div class="pro-coding"><label>商品编码：</label><span id="pro-sku-code">10110010133601</span></div>
	                    <div class="pro-evaluate"><label>商品评分：</label><span class="pro-star"><span class="starRating-area"><s id="prd-remark-scoreAverage" style="width:100%"></s></span></span>&nbsp;（<a id="prd-remark-jmptoremark" href="#pro-tab-evaluate" onclick="ec.product.jmptoRemark()"   title="共 0 条评论">共&nbsp;0&nbsp;条评论</a>）</div>
			    		
				<div class="pro-freight"><label>运&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;费：</label>
				<span style="color:red;">满&nbsp;99&nbsp;免运费</span>

				</div>
				<div class="pro-service"><label>服&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;务：</label>由华为商城负责发货，并提供售后服务</div>
				<div class="pro-service"><label>以旧换新：</label><a href="http://www.vmall.com/recycle?url=%3Fpid%3D1032" target="_blank" textvalue="旧机最高抵5000元，立即参加&gt;&gt;"><span style="color:#ff0000"><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:red;font-size:14px"><span style="font-family:arial, helvetica,sans-serif;color:red;font-size:12px">旧机最高抵</span><span style="font-family:arial, helvetica,sans-serif;color:red;font-size:12px">5000元，</span></span>立即参加&gt;&gt;</span></a></div>	 	
			    <div class="hr-10"></div>
			    <div class="line"></div>
			    <div class="hr-15"></div>
						
	                    <!--商品简介-SKU -->
	                    <div id="pro-skus" class="pro-sku-area">加载中...</div>
						
						<div class="pro-sku-area hide" id="contractLst">
							<!-- 联通合约机套餐 -->	
							<dl class="pro-sku-text clearfix " >
									<dt>合&nbsp;&nbsp;约&nbsp;&nbsp;机：</dt>
									<dd id="contractList-ol">
									</dd>
							</dl>
							<form action="/contract/choose-{id}" id="contractForm" class="hide"></form>
						</div>
						 
						 
						<!-- 20140710-商品简介-延保-start -->
						<div class="pro-ew-area hide">
							<dl class="pro-sku-text clearfix">
								<dt>购买服务：</dt>
								<dd>
									<ol class="clearfix">
										<!-- 鼠标悬停追加ClassName： hover，选中追加Class Name：selected -->
										<li id="extendProtected" class="sku-select">
											<!-- 取数据字节最长的div.sku的宽度赋值给所有div.sku -->
											<div style="width:160px;" class="sku">
											   <a href="javascript:;" title="延保服务">华为延保商品A类服务
											   </a><i></i><s></s>
											</div>
											<ul >
									
											</ul>
											
										</li>
										<li id=extendProtectedProduct> 
											<div class="pro-sku-tips">
												<a href="javascript:;" target="_blank">详情 ></a>
											</div>
										</li>
										<li id="accidentProtected" class="sku-select clear">
											<!-- 取数据字节最长的div.sku的宽度赋值给所有div.sku -->
											<div style="width:160px;" class="sku">
											      <a href="javascript:;" title="意外保服务">华为意外保商品B类服务
											      </a><i></i><s></s>
											</div>
											<ul>
												
											</ul>
											<div class="pro-sku-tips">
											</div>
										</li>
										
										<li id=accidentProtectedProduct> 
											<div class="pro-sku-tips">
												<a href="javascript:;" target="_blank">详情 ></a>
											</div>
										</li>
									
									</ol>
								</dd>
							</dl>
						</div><!-- 20140710-商品简介-延保-end -->
						
	                    <!-- 关联商品 -->					
						<div class="pro-sku-area" id="relPrdList">
							<dl class="pro-sku-text clearfix">
								<dt>关联商品：</dt>
								<dd>
									<ol>
										<li data-id="2642"><div class="sku"><a title="HUAWEI WATCH 智能手表 W1" href="/product/2642.html" target="_blank">HUAWEI WATCH 智能手表 W1</a><s></s></div></li>
										<li data-id="3419"><div class="sku"><a title="HUAWEI P9 Plus" href="/product/3419.html" target="_blank">HUAWEI P9 Plus</a><s></s></div></li>
										<li data-id="3457"><div class="sku"><a title="华为 HUAWEI Type C 转接头" href="/product/3457.html" target="_blank">华为 HUAWEI Type C 转接头</a><s></s></div></li>
										<li data-id="538499714"><div class="sku"><a title="华为路由 Q1 子母套装" href="/product/538499714.html" target="_blank">华为路由 Q1 子母套装</a><s></s></div></li>
									</ol>
								</dd>
							</dl>
						</div>
	                    
	                    <!-- 20131215-商品简介-购买数量-start -->
						<div id="pro-quantity-area"  class="pro-stock-area">
							<dl class="clearfix">
								<dt>购买数量：</dt>
								<dd>
								 <span class="stock-area">
							     <input id="pro-quantity" type="text" class="vam text" value="1" autocomplete="off" />
	   						     </span>
								</dd>
							</dl>
						</div><!-- 20131215-商品简介-购买数量-end -->                 
	                    
	                    <!--  商品简介-购买数量
						<div id="pro-quantity-area" class="pro-stock-area" style="display: none;">购买数量：
	    					<span class="stock-area">
							<input id="pro-quantity" type="text" class="vam text" value="1" autocomplete="off" />
	   						</span>
						</div>
	                      -->
	                    <!-- dbank温馨提示 -->
	                    <div class="pro-tips-area"  id="pro-msg" style="display: none;" >
	                    	<div class="tips-style-2 tips-area">
	                    		<i></i>
	                    		 <div class="tips-text"><p><span>温馨提示：</span><span id="pro-msg-title">本套餐只适用于华为网盘标准和VIP用户购买，至尊VIP用户请勿购买。</span></p></div>
							</div>
						</div><!-- 20131218-商品简介-tips-end -->
						
	                </div><!--end pro-meta-area-->
	                
			<!--商品简介-提交操作 -->
			<form id="order-confirm-form" method="post" class="hide" action="/order/nowConfirmcart"></form>
			<!--延保单独购买提交form -->
			<form id="order-confirm-extend-form" method="post" class="hide" action="/order/extendConfirm"></form>
			<form id="order-shoppingCart-form" method="get" class="hide" action="http://cart.vmall.com/cart/cart.html"></form>
			<!--预付订金form -->
			<form id="order-confirm-deposit-form" method="post" class="hide" action="/order/confirmDeposit"></form>
			<input id="isPriority" name="isPriority" type="hidden" value="0"/>
			<script src="js/detail.min.js"></script>
	                
	 

	<!--商品简介-提交操作 -->
	<div class="pro-fixed-action">
	    <div id="pro-select-sku" class="pro-selected"></div>
	    <div id="pro-operation" class="pro-action-area"></div>
	    <div class="pro-agreement-area hide" id="pro-agreement-area">
			<input class="vam" checked type="checkbox" id="pro-agreement-area-check"><label for="" class="vam">同意</label>&nbsp;<a href="javascript:;" class="vam" onclick="ec.product.showDepositAgreement()">订金支付协议</a>
		</div>
	</div>

	 
	<!--弹出层-成功添加到购物车 -->
	<div id="cart-tips" class="pro-popup-area hide">
	     <div class="h">
		<a href="javascript:;" onclick="$('#cart-tips').hide()" class="pro-popup-close" title="关闭"><span>关闭</span></a>
	    </div>
	    <div class="b">
			<div class="pro-add-success">
				<dl>
				<dt><s></s></dt>
					<dd>
						<p>HUAWEI P9 双摄像头  指纹识别</p>
						<div class="pro-add-success-msg">成功加入购物车!</div>
						<div class="pro-add-success-total">购物车中共&nbsp;<b id="cart-total">0</b>&nbsp;件商品，金额合计&nbsp;<b  id="cart-price">¥&nbsp;0</b></div>
						<div class="pro-add-success-button"><a href="javascript:;" class="button-style-1 button-go-cart" title="去购物车结算" onclick="ec.product.gotoshoppingCart()">去结算</a><a class="button-style-4 button-walking" href="javascript:;" title="继续逛逛" onclick="$('#cart-tips').hide()">继续逛逛&nbsp;>></a></div>									
					</dd>
				</dl>					
			</div>
	    </div>
	</div>

	<!--弹出层-提示信息 -->
	<div id="popup-tips" class="pro-popup-area hide">
	    <div class="h">
		<a href="javascript:;" class="pro-popup-close" title="关闭" onclick="$('#popup-tips').hide()"><span>关闭</span></a>
	    </div>
	    
	    <div class="b">
			<div class="pro-add-error">
				<div class="pro-add-error-msg" id="popup-tips-msg" >非常抱歉！该商品不能单独销售！</div>
				<div class="pro-add-error-button"><a class="button-style-1 button-par-define" href="javascript:;" onclick="$('#popup-tips').hide()" title="知道了">知道了</a></div>
			</div>
	    </div>
	</div>
	    
	<!-- 20130913-弹出层-购买延保-start -->
	<div id="popup-extend" class="pro-popup-area hide">
		<div class="h">
			<a href="javascript:;" class="pro-popup-close" title="关闭" onclick="$('#popup-extend').hide()">关闭</a>
		</div>
		
		<div class="b">
			<div class="pro-extend-area">
					<h3>购买延保</h3>
					<div class="pro-extend-search">
						<div class="form-edit-area">
							<form action="javascript:;" id="checkExtend-ID" onsubmit="return ec.product.checkExtend()">
								<input type="hidden" id="extendSkuId" />
								<input type="text" class="text vam" id="extend-text" maxlength="50" /><input class="button-style-4 button-extend-search vam" type="submit" value="查看可购买的延保" />
							</form>
						</div>
					</div>
				
					<div class="pro-extend-result hide"  id="pro-extend-result-id">
						<div id="extend-msg-succuss" class="hide"></div>
						<div id="extend-msg-error" class="hide"></div>
					</div>
					
					<div class="pro-extend-tips">温馨提示：IMEI/SN/MEID号可在产品外包装上找到，可拆卸电池的手机可将电池拆掉电池下面的标签上可以看到，手机上输入*#06#也可以显示。</div>
					<div class="pro-extend-button">
						<a id="button-extend" class="button-style-disabled-1 button-go-extend-checkout-disabled" href="javascript:;" title="提交订单" onclick="ec.product.extendBuy(this)"><span>提交订单</span></a>
					</div>
			</div>
		</div>

	</div><!-- 20130913-弹出层-购买延保-end -->

	<!-- Baidu Button BEGIN -->
	<div class="pro-bdShare-area">
		<div id="bdshare" class="bdShare bdshare_t bds_tools get-codes-bdshare" data="{'url':'http://www.vmall.com/product/3325.html'}">
		</div>
	</div>
	<form action="#" id="gotourl" method="get"></form>
	<script>
		ec.code.addShare({
			title : "我在@华为商城 发现了一个非常不错的商品：HUAWEI P9 4GB+64GB 全网通版（金色） 华为价：￥ 3688.00。分享一下："
		});

		//去结算
		ec.product.gotoshoppingCart = function(){
			//BI统计
			_paq.push(['trackLink','加入购物车-去支付', 'link', ' ']);
			ec.code.addAnalytics({hicloud:true});
			setTimeout(function () {
				//var url = '/order/shoppingCart';
				var url = 'http://cart.vmall.com/cart/cart.html';
				$('#gotourl').attr('action', url).submit();
			}, 500);
		};
	</script>
	<!-- Baidu Button END -->            </div>
	        </div>
	        
	        <div class="left-area">
	        	<!--商品简介-图片画廊 -->
	        	<div class="pro-gallery-area">
	            	<div class="pro-gallery-img">
						<a id="product-img" href='http://res.vmallres.com/pimages//product/6901443118847/800_800_1467252520756mp.jpg' class = 'cloud-zoom'  rel="adjustX: 10, adjustY:0, zoomWidth:400 , zoomHeight:400">
							<img src="images/428_428_1467252520756mp.jpg" alt="HUAWEI P9"/>
						</a>
					</div>
	                <div class="pro-gallery-nav">
	                	<a href="javascript:;" class="pro-gallery-back" onclick="ec.product.imgSlider.prev()"></a>
	                	<div class="pro-gallery-thumbs">
	                        <ul id="pro-gallerys">
		                        <li class="current">
		                            <a href="javascript:;"><img src="images/55_55_1467252520756mp.jpg" alt="HUAWEI P9"/></a>
		                        </li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249557057.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249560122.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249566489.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249569195.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249563584.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249589893.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249595386.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249592572.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249583962.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249586302.jpg"  alt="HUAWEI P9"/></a>
								</li>
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249598629.jpg"  alt="HUAWEI P9"/></a>
								</li>
							<!--修改套图的拼接方式 
								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249557057.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249560122.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249566489.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249569195.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249563584.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249589893.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249595386.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249592572.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249583962.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249586302.jpg"  alt="HUAWEI P9"/></a>
								</li>

								<li>
	                        		<a href="javascript:;"><img src="images/55_55_1467249598629.jpg"  alt="HUAWEI P9"/></a>
								</li>
							-->
	                        </ul>
	                    </div>
	                    <a href="javascript:;" class="pro-gallery-forward" onclick="ec.product.imgSlider.next()"></a>
	                </div>
	            </div>
	        </div>
	        
	    </div>
	</div>


	<div id="group-area" class="layout hide">
	<div class="hr-20"></div>
		    <!--商品详情-优惠套装 -->
		    <div id="tab-bundle" class="pro-suit-area">
			<div class="h">
			    <h3>优惠套装</h3>
			    <div class="h-tab" id="bundle-tab">
			    </div>
			</div>
			<div class="b clearfix">
			    <div class="pro-main">
				<!--tab切换以pro-suit-parts-list作为模块进行切换- pro-suit-parts-list的style属性 overflow-x: scroll当子集li的N数大于5时出现-->

			    </div>
				<div class="pro-sub">
					<div class="pro-suit-cost-area">
			    	</div>
			    </div>
			</div>
		    </div>
		    <div id="tab-split" class="hr-20 hide"></div>
		    <!--商品详情-优惠套装 end -->

		    <!--商品详情-搭配推荐 -->
			<div id="tab-comb" class="pro-recommend-area">
			  <div class="h">
	            <h3>搭配推荐</h3>
				<div class="h-tab">
	               		 <ul id="combList-list-8255" data-size="2" class="clearfix">
	                    	<li class="current" data-id="609">
							<a href="javascript:;" title="推荐搭配一" onmouseover="ec.product.combChange(this)"><span>推荐搭配一</span></a>
							</li>
	                    	<li class="" data-id="675">
							<a href="javascript:;" title="推荐搭配二" onmouseover="ec.product.combChange(this)"><span>推荐搭配二</span></a>
							</li>
	              	  </ul>
	               	<ul id="combList-list-8259" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="659">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-8261" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="611">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-8263" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="613">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-8265" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="617">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-8267" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="615">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-8269" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="619">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-8273" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="623">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-8277" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="703">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-8279" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="701">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	               	<ul id="combList-list-721625417" data-size="1" class="clearfix" style="display:none;">
	                    	<li class="current" data-id="677">
							<a href="javascript:;" title="推荐搭配" onmouseover="ec.product.combChange(this)"><span>推荐搭配</span></a>
							</li>
	                </ul>
	            </div>
	        </div>
			<div class="b clearfix">
			    <div class="pro-main" id="comb-pro-area">
					<div id="comb-pro-8255" class="pro-suit-parts-list" >
					    <div class="pro-suit-parts-area" style="width:1002px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467252520756mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;3688.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 4GB+64GB 全网通版（金色）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3321.html#8227" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3321.html#8227" title="">HUAWEI P9 小牛皮皮套（棕色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;299.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8227" autocomplete="off" data-price="299" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3321.html#8225" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3321.html#8225" title="">HUAWEI P9 小牛皮皮套（黑色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;299.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8225" autocomplete="off" data-price="299" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8215" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8215" title="">HUAWEI P9 无边开窗皮套（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8215" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8255" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:1002px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467252520756mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;3688.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 4GB+64GB 全网通版（金色）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3225.html#7853" title="" target="_blank"><img src="images/100_100_1466654835972mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3225.html#7853" title="">华为 HUAWEI 主动降噪耳机2（香槟金）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;599.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="7853" autocomplete="off" data-price="599" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3487.html#8739" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3487.html#8739" title="">华为手环 B3 商务版（摩卡棕）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;1199.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8739" autocomplete="off" data-price="1199" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8219" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8219" title="">HUAWEI P9 无边开窗皮套（深灰色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8219" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3321.html#8229" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3321.html#8229" title="">HUAWEI P9 小牛皮皮套（蓝色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;299.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8229" autocomplete="off" data-price="299" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3153.html#7591" title="" target="_blank"><img src="images/100_100_1466651783642mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3153.html#7591" title="">华为5000mAh 移动电源（石墨黑）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;79.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="7591" autocomplete="off" data-price="79" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8259" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:1002px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467252608007mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;3688.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 4GB+64GB 全网通版（陶瓷白）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8215" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8215" title="">HUAWEI P9 无边开窗皮套（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8215" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3444243.html#523383389" title="" target="_blank"><img src="images/100_100_1462247649464mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3444243.html#523383389" title="">HUAWEI P9 莱卡定制皮套（酒红色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;349.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="523383389" autocomplete="off" data-price="349" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8217" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8217" title="">HUAWEI P9 无边开窗皮套（粉色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8217" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8261" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:835px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467252677697mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;3688.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 4GB+64GB 全网通版（玫瑰金）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3444243.html#523383389" title="" target="_blank"><img src="images/100_100_1462247649464mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3444243.html#523383389" title="">HUAWEI P9 莱卡定制皮套（酒红色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;349.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="523383389" autocomplete="off" data-price="349" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8217" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8217" title="">HUAWEI P9 无边开窗皮套（粉色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8217" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8263" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:835px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467252774332mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;3188.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 3GB+32GB 全网通版（皓月银）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3444243.html#523383389" title="" target="_blank"><img src="images/100_100_1462247649464mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3444243.html#523383389" title="">HUAWEI P9 莱卡定制皮套（酒红色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;349.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="523383389" autocomplete="off" data-price="349" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8221" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8221" title="">HUAWEI P9 无边开窗皮套（金色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8221" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8265" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:835px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467252867876mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;3188.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 3GB+32GB 全网通版（钛银灰）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3444243.html#523383389" title="" target="_blank"><img src="images/100_100_1462247649464mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3444243.html#523383389" title="">HUAWEI P9 莱卡定制皮套（酒红色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;349.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="523383389" autocomplete="off" data-price="349" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8219" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8219" title="">HUAWEI P9 无边开窗皮套（深灰色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8219" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8267" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:835px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467252927907mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;3188.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 3GB+32GB 全网通版（流光金）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8221" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8221" title="">HUAWEI P9 无边开窗皮套（金色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8221" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3321.html#8227" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3321.html#8227" title="">HUAWEI P9 小牛皮皮套（棕色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;299.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8227" autocomplete="off" data-price="299" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8269" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:835px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467253007932mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;2988.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 3GB+32GB 电信定制版（皓月银）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3444243.html#523383389" title="" target="_blank"><img src="images/100_100_1462247649464mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3444243.html#523383389" title="">HUAWEI P9 莱卡定制皮套（酒红色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;349.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="523383389" autocomplete="off" data-price="349" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8221" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8221" title="">HUAWEI P9 无边开窗皮套（金色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8221" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8273" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:835px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467253265493mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;2988.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 3GB+32GB 移动定制版（皓月银）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3321.html#8229" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3321.html#8229" title="">HUAWEI P9 小牛皮皮套（蓝色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;299.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8229" autocomplete="off" data-price="299" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8221" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8221" title="">HUAWEI P9 无边开窗皮套（金色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8221" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8277" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:1002px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467253885714mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;2988.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 3GB+32GB 联通定制版（皓月银）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8215" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8215" title="">HUAWEI P9 无边开窗皮套（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8215" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3321.html#8229" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3321.html#8229" title="">HUAWEI P9 小牛皮皮套（蓝色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;299.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8229" autocomplete="off" data-price="299" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3444243.html#523383389" title="" target="_blank"><img src="images/100_100_1462247649464mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3444243.html#523383389" title="">HUAWEI P9 莱卡定制皮套（酒红色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;349.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="523383389" autocomplete="off" data-price="349" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-8279" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:1002px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467254147578mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;2988.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 3GB+32GB 联通定制版（钛银灰）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3321.html#8225" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3321.html#8225" title="">HUAWEI P9 小牛皮皮套（黑色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;299.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8225" autocomplete="off" data-price="299" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3444243.html#523383389" title="" target="_blank"><img src="images/100_100_1462247649464mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3444243.html#523383389" title="">HUAWEI P9 莱卡定制皮套（酒红色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;349.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="523383389" autocomplete="off" data-price="349" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8223" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8223" title="">HUAWEI P9 无边开窗皮套（棕色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8223" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
					<div id="comb-pro-721625417" class="pro-suit-parts-list hide" >
					    <div class="pro-suit-parts-area" style="width:1002px;">
						<ul class="clearfix">
							    <li>
								<div class="suit-master-area">
								    <p class="p-img"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别"><img src="images/100_100_1467254679960mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-price"><em id="mainCombPrice">华为价：&yen;3688.00</em></p>
								    <p class="p-name"><a href="javascript:;" title="HUAWEI P9 双摄像头  指纹识别">HUAWEI P9 4GB+64GB 全网通版（琥珀灰）</a></p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3321.html#8225" title="" target="_blank"><img src="images/100_100_1.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3321.html#8225" title="">HUAWEI P9 小牛皮皮套（黑色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;299.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8225" autocomplete="off" data-price="299" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3319.html#8219" title="" target="_blank"><img src="images/100_100_1 (1).jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3319.html#8219" title="">HUAWEI P9 无边开窗皮套（深灰色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;229.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8219" autocomplete="off" data-price="229" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3444243.html#523383389" title="" target="_blank"><img src="images/100_100_1462247649464mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3444243.html#523383389" title="">HUAWEI P9 莱卡定制皮套（酒红色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;349.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="523383389" autocomplete="off" data-price="349" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3457.html#8695" title="" target="_blank"><img src="images/100_100_1466652827627mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3457.html#8695" title="">华为 HUAWEI Type C 转接头（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;14.90 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8695" autocomplete="off" data-price="14.9" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
							    <li>
								<div class="suit-area">
								    <p class="p-img"><a href="/product/3301.html#8317" title="" target="_blank"><img src="images/100_100_1466652784051mp.jpg" alt="HUAWEI P9 双摄像头  指纹识别"/></a></p>
								    <p class="p-name"><a href="/product/3301.html#8317" title="">华为 HUAWEI Type C 数据线（白色）</a></p>
								    <p onclick="ec.product.combClick(this)" class="p-price"> 
								    	<s></s> 
								    	<em>&yen;29.00 </em> 
								    	<input class="hide" type="checkbox" name="skuId" value="8317" autocomplete="off" data-price="29" class="vam"/>
								    </p>
								    <i class="and"></i>
								</div>
							    </li>
						</ul>
					    </div>
					</div>
			    </div>
			    <div class="pro-sub">
					<div class="pro-suit-cost-area">
			            <p>已搭配：<em id="comb-count">0</em>&nbsp;件</p>
				    	<p><b>组合价：&yen;<span id="comb-price"></span></b></p>
				    	<p class="pro-suit-cost-button"><a href="javascript:;" class="button-add-cart-2 button-style-1" title="加入购物车" onclick="ec.product.buyComb();return false;"><span>加入购物车</span></a></p>
						<p class="pro-suit-cost-agreement hide" id="pro-suit-cost-agreement-area"> <input checked type="checkbox" class="vam" id="pro-suit-cost-agreement-area-check"><label class="vam" for="">同意</label>&nbsp;<a href="javascript:;" class="vam" onclick="ec.product.showDepositAgreement()">订金支付协议</a></p>
					</div>
				</div>
			<div class="f"></div>
		    </div>
		    <!--商品详情-搭配推荐 end -->
	</div>
	</div>


	<div class="hr-20"></div>
	<div class="layout">
		<div class="fr u-3-4">
		<!-- 20131220-商品详情-start -->
			<div class="pro-detail-area clearfix">
			<div class="tool-fixed-holder"></div>
			<div id="pro-tab-all" class="pro-detail-tool">
				
			<!--商品详情-书签 --> 
	        <div id="pro-tab-area" class="pro-detail-tab clearfix">
	        	<div class="pro-detail-tab-nav">
	            <ul>
	                <li id="pro-tab-feature" class="current"><a href="javascript:;" title="商品详情"><span>商品详情</span></a></li>
	                <li id="pro-tab-evaluate"><a href="javascript:;" title="用户评价"><span id="prd-remark-span-tab-evaluate">用户评价<em>（0）</em></span></a></li>
	                <li id="pro-tab-parameter" data-skulist="8255,8259,8261,8263,8265,8267,8269,8273,8275,8277,8279,721625417"><a href="javascript:;" title="规格参数"><span>规格参数</span></a></li>
	                <li id="pro-tab-package"><a href="javascript:;" title="包装清单"><span>包装清单</span></a></li>
			
	                <li id="pro-tab-service"><a href="javascript:;" title="售后服务"><span>售后服务</span></a></li>
	              
	            </ul>
	            </div>
	            
	            <div  class="pro-detail-tab-button"><a id="tab-addcart-button" href="javascript:;" class="button-style-1 button-add-cart-3" onclick="ec.product.buyComb();return false;">加入购物车</a><a href="javascript:;" id="tab-notice-button" class="button-style-2 button-notice-arrival-2"  onclick="ec.product.arrival();return false;">到货通知</a></div>
	        </div>
	        </div>

	        <!--商品详情-->
	        <div class="pro-detail-area">
	            <!--产品特征 -->
	<div id="pro-tab-feature-content" class="pro-detail-tab-area pro-feature-area">
	<div>
	<div id="pro-tab-feature-content-8255" class="hide">
	<p><img src="images/201606141013405737753.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102928579.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/20160507102928739.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507102928586.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102928856.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102929430.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102928995.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102929927.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102929443.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102929636.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102929789.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507102929834.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size: 14px;"><strong>P9 插卡指导视频</strong>：<span style="font-family: 宋体;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA"><span style="font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</span></a> </span></span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8259" class="hide">
	<p><img src="images/20160614101358650377.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102855304.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/20160507102855469.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507102855245.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102855410.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102855747.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102855688.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102855880.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102855905.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102855999.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102856155.jpg" style="" title="1_10.jpg"/></p><p><img src="images/2016050710285678.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8261" class="hide">
	<p><img src="images/201606141014249484651.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102816821.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/20160507102817152.jpg" style="" title="1_02.jpg"/></p><p><img src="images/2016050710281731.jpg" style="" title="1_03.jpg"/></p><p><img src="images/2016050710281726.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102817360.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102817314.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102817601.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102817552.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102817771.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102817772.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507102817855.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8263" class="hide">
	<p><img src="images/201606141014405028964.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102636689.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/20160507102636638.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507102636671.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102636869.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102636914.jpg" style="" title="1_05.jpg"/></p><p><img src="images/2016050710263777.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102637140.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102637141.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102637368.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102637409.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507102637339.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8265" class="hide">
	<p><img src="images/201606141015168138206.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102554969.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/20160507102555191.jpg" style="" title="1_02.jpg"/></p><p><img src="images/2016050710255576.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102555117.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102555572.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102555621.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102555625.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102555749.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102555847.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102555908.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507102555908.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8267" class="hide">
	<p><img src="images/201606141014583760736.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102522926.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/2016050710252359.jpg" style="" title="1_02.jpg"/></p><p><img src="images/2016050710252306.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102523195.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102523333.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102523286.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102523493.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102523468.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102523542.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102523640.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507102523651.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8269" class="hide">
	<p><img src="images/201606141015596127518.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102446297.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/20160507102446372.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507102446491.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102446514.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102446655.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102447271.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102447305.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102447266.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102447736.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102447700.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507102447693.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8273" class="hide">
	<p><img src="images/201606141016369591253.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102332372.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/20160507102332527.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507102332971.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102332535.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102332950.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102332693.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102333394.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102333241.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102333357.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102333448.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507102333608.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8275" class="hide">
	<p><img src="images/20160614101620705012.jpg" title="9.jpg" alt="9.jpg"/></p><p><img src="images/20160507102236875.jpg" style="" title="1_01.jpg"/></p><p><img src="images/20160507102237339.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507102236964.jpg" style="" title="1_03.jpg"/></p><p><img src="images/2016050710223736.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102237197.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102237295.jpg" style="" title="1_06.jpg"/></p><p><img src="images/2016050710223811.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102237510.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102237627.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102237727.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507102237861.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8277" class="hide">
	<p><img src="images/201606141017096426368.jpg" title="9.jpg" alt="9.jpg"/></p><p><img src="images/20160507102104452.jpg" style="" title="1_01.jpg"/></p><p><img src="images/20160507102104486.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507102104492.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102104626.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102104687.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102104667.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102104775.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102104876.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102104913.jpg" style="" title="1_09.jpg"/></p><p><img src="images/2016050710210542.jpg" style="" title="1_10.jpg"/></p><p><img src="images/2016050710210566.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-8279" class="hide">
	<p><img src="images/201606141016536918081.jpg" title="9.jpg" alt="9.jpg"/></p><p><img src="images/20160507101936422.jpg" style="" title="1_01.jpg"/></p><p><img src="images/20160507101936558.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507101936527.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507101936635.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507101936763.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507101936733.jpg" style="" title="1_06.jpg"/></p><p><img src="images/2016050710193753.jpg" style="" title="1_07.jpg"/></p><p><img src="images/2016050710193735.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507101937240.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507101937210.jpg" style="" title="1_10.jpg"/></p><p><img src="images/20160507101937265.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	<div id="pro-tab-feature-content-721625417" class="hide">
	<p><img src="images/201606141017253993899.jpg" title="9.jpg" alt="9.jpg"/></p><p><img alt="1_01.jpg" src="images/20160507102855304.jpg" style="float:none;" title="1_01.jpg"/></p><p><img src="images/20160507102855469.jpg" style="" title="1_02.jpg"/></p><p><img src="images/20160507102855245.jpg" style="" title="1_03.jpg"/></p><p><img src="images/20160507102855410.jpg" style="" title="1_04.jpg"/></p><p><img src="images/20160507102855747.jpg" style="" title="1_05.jpg"/></p><p><img src="images/20160507102855688.jpg" style="" title="1_06.jpg"/></p><p><img src="images/20160507102855880.jpg" style="" title="1_07.jpg"/></p><p><img src="images/20160507102855905.jpg" style="" title="1_08.jpg"/></p><p><img src="images/20160507102855999.jpg" style="" title="1_09.jpg"/></p><p><img src="images/20160507102856155.jpg" style="" title="1_10.jpg"/></p><p><img src="images/2016050710285678.jpg" style="" title="1_11.jpg"/></p><p style="text-align: center;"><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">P9 </span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">插卡指导视频：</span><span style="font-size:14px;font-family:&#39;MS Gothic&#39;"></span><span style="font-size:14px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;"><a href="http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA">http://v.youku.com/v_show/id_XMTUyNjg0MDQ2MA</a> </span></p><p><br/></p>
	</div>
	</div>
	<div class="hr-20"></div>
	<div class="pro-disclaimer-area">
	<p>※本网站尽可能地提供准确的信息。本网站内所涉及的产品图片与实物可能有细微区别，效果演示图和示意图仅供参考（图片为合成图、模拟演示图），有关产品的外观（包括但不限于颜色）请以实物为准。</p>
	<p>※限于篇幅，本网站中所包含的信息（包括但不限于产品规格、功能说明等）可能不完整，请以有关产品使用说明书的具体信息为准。</p>
	</div>
	</div>
	<!--规格参数 -->
	<div id="pro-tab-parameter-content" class="pro-detail-tab-area pro-parameter-area hide"><div>正在加载中...</div></div>
	<!--包装清单 -->
	<div id="pro-tab-package-content" class="pro-detail-tab-area pro-package-area hide">
	<div id="pro-tab-package-content-8255">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8259">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8261">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8263">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8265">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8267">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8269">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8273">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8275">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8277">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-8279">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	<div id="pro-tab-package-content-721625417">
	<p><span style="font-family:arial,helvetica,sans-serif;font-size:12px;">手机（电池内置） x 1，中式充电器 x 1，数据线（Type-C头） x 1，半入耳式线控耳机 x 1，点纹透明保护壳 x 1，快速指南 x 1，华为手机三包凭证 x 1，取卡针 x 1，金卡会员卡 x 1</span><br/></p><p><br/></p>
	</div>
	</div>
	<!--售后服务 -->
	<div id="pro-tab-service-content" class="pro-detail-tab-area pro-service-area hide">
	<div id="pro-tab-service-content-8255" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8259" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8261" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8263" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8265" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8267" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8269" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8273" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8275" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8277" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-8279" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	<div id="pro-tab-service-content-721625417" class="hide">
	<p><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">本产品全国联保，享受三包服务，质保期为：一年质保</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">如因质量问题或故障，凭厂商维修中心或特约维修点的质量检测证明，享受7天内退货，15日内换货，15日以上在质保期内享受免费保修等三包服务！</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">售后服务电话：400-830-8300</span><br style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"/><span style="line-height:21px;background-color:#ffffff;font-family:tahoma, 微软雅黑;font-size:14px"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">华为消费者BG官网： </span><a style="font-family:arial, helvetica, sans-serif;color:#333333;cursor:pointer;text-decoration:underline" href="http://consumer.huawei.com/cn/"><span style="line-height:21px;background-color:#ffffff;font-family:arial,helvetica,sans-serif;font-size:12px;">http://consumer.huawei.com/cn/</span></a></span><br/></p>
	</div>
	</div>	    <div id="remarkLoading"></div>
	            <!--用户评价 -->
	            <div id="pro-tab-evaluate-content" class="pro-detail-tab-area pro-evaluate-area  hide">
	                <!--用户评分 -->
	                <div class="pro-score-area clearfix">
	                	<div class="pro-score-average">
	                	    <em>好评度:</em>
							<span><b id="pro-evaluate-avgSorce"></b>%</span>
							
						</div>
						
	                 <div id="pro-score-impress"  class="pro-score-impress"></div>
	                  
	                   <div class="pro-score-button"><a href="/member/order/" class="button-style-4 button-comment">发表评价</a></div>
	                </div>
	                
	                <!-- 20131220-商品详情-用户评论-书签-start -->
					<div class="pro-evaluate-tab clearfix">
						<div class="pro-evaluate-tab-nav">
							<ul id="pro-evaluate-click-type">
								<li id="pro-evaluate-click-all" class="current"><a href="javascript:;"><span>全部评价<em id="pro-evaluate-number-all">(</em></span></a></li>
								<li id="pro-evaluate-click-high"><a  href="javascript:;"><span>好评<em id="pro-evaluate-number-high" >(100)</em></span></a></li>
								<li id="pro-evaluate-click-mid" ><a href="javascript:;"><span  >中评<em id="pro-evaluate-number-mid"></em></span></a></li>
								<li id="pro-evaluate-click-low"><a href="javascript:;"><span  >差评<em id="pro-evaluate-number-low"></em></span></a></li>
							</ul>
						</div>
						
						<!--分页 -->
	                <div class="pro-evaluate-page">
						<!-- 20131220-分页-start -->
						<div id="pro-msg-pagerup" class="pager">
						</div><!-- 20131220-分页-end -->
					</div>
						
					</div><!-- 20131220-商品详情-用户评价-书签-end -->
					
	                <!--用户留言 -->
	                <!-- 20131222-商品详情-用户评价-start -->
					<div id="pro-msg-list"  class="pro-comment-list">
					</div><!-- 20131222-商品详情-用户评价-end -->
					
	               
	                <!--分页 -->
	                <div class="pro-evaluate-page">
						<!-- 20131220-分页-start -->
						<div id="pro-msg-pager" class="pager">
							
						</div><!-- 20131220-分页-end -->
						 	<div class="hr-15"></div>
						</div>
	            	</div>
	        	</div>
	    	</div>
	    	<!--咨询-->
	    <div class="pro-detail-area clearfix" id="prd_detail_counsel">
				<div class="pro-detail-tool">
					<!-- 20140624-商品详情-书签-start -->
					<div class="pro-detail-tab clearfix">
						<div class="pro-detail-tab-nav">
							<ul>
								<li class="current" id="prd_detail_counsel_1"><a href="javascript:;" onclick="ec.product.divchange(1)"><span>全部咨询</span></a></li>
								<li id="prd_detail_counsel_6"><a href="javascript:;" onclick="ec.product.divchange(6)"><span>常见问题</span></a></li>
								<li id="prd_detail_counsel_2"><a href="javascript:;" onclick="ec.product.divchange(2)"><span>商品咨询</span></a></li>
								<li id="prd_detail_counsel_3"><a href="javascript:;" onclick="ec.product.divchange(3)"><span>支付</span></a></li>
								<li id="prd_detail_counsel_4"><a href="javascript:;" onclick="ec.product.divchange(4)"><span>配送</span></a></li>
								<li id="prd_detail_counsel_5"><a href="javascript:;" onclick="ec.product.divchange(5)"><span>售后</span></a></li>
								
							</ul>
						</div>
						<div class="pro-detail-tab-link"><a href="#inquire-form" onclick="ec.product.loginCheckBefCoun();">发表咨询&gt;&gt;</a></div>
					</div><!-- 20140624-商品详情-书签-end -->
				</div>

				<!-- 20140624-商品详情-全部咨询-start -->
				<div class="pro-detail-tab-area pro-inquire-area " id="prddetail_counsel_all">
					<!-- 20140624-商品详情-咨询提示-start -->
					<div class="pro-inquire-tips">
						<label>温馨提示：</label>因产线可能更改商品包装、产地、附配件等未及时通知，且每位咨询者购买、提问时间等不同。为此，客服给到的回复仅对提问者3天内有效，其他网友仅供参考！给您带来的不便还请谅解，谢谢！
					</div><!-- 20140624-商品详情-咨询提示-end -->
					<div class="pro-inquire-list" id="all_prd_counsel_content">
						<div class="pro-inquire-item clearfix">
							<div class="pro-inquire-user">
								<label>网友：</label><span>13820*****</span><s><i class="icon-vip-level-2"></i></s><em>2016-07-19 21:54:07</em>
							</div>
							<div class="pro-inquire-question">
								<label>咨询内容：</label><span>请问此款支持nfc  吗？</span>
							</div>
							<div class="pro-inquire-answer">
								<label>回复：</label><span>您好，欢迎咨询华为商城。很抱歉，p9不支持nfc，感谢您对华为商城的支持。</span>
								<em>2016-07-20 09:51:22</em>
							</div>
						</div>
						<div class="pro-inquire-item clearfix">
							<div class="pro-inquire-user">
								<label>网友：</label><span>放羊娃</span><s><i class="icon-vip-level-3"></i></s><em>2016-07-19 20:11:54</em>
							</div>
							<div class="pro-inquire-question">
								<label>咨询内容：</label><span>荣耀8和荣耀V8的区别？</span>
							</div>
							<div class="pro-inquire-answer">
								<label>回复：</label><span>您好，荣耀V旗舰系列和荣耀年度旗舰系列，都致力于成为最受年轻人喜爱的科技和美学旗舰。荣耀V系列是搭载荣耀最新科技的全新系列，荣耀V8定位极致科技。荣耀8是拥有旗舰性能的科技美学旗舰，主打潮流美学。谢谢。</span>
								<em>2016-07-20 09:59:28</em>
							</div>
						</div>
						<div class="pro-inquire-item clearfix">
							<div class="pro-inquire-user">
								<label>网友：</label><span>huafans01187897352</span><s><i class="icon-vip-level-0"></i></s><em>2016-07-19 19:59:04</em>
							</div>
							<div class="pro-inquire-question">
								<label>咨询内容：</label><span>华为p9全网通支持移动和电信双卡双待双通吗？谢谢！</span>
							</div>
							<div class="pro-inquire-answer">
								<label>回复：</label><span>您好，欢迎咨询华为商城。全网通版主卡支持移动4G/3G/2G，联通4G/3G/2G，电信4G/3G/2G；副卡槽支持移动2G、联通2G，电信3g/2g，p9是双卡双待双通的，感谢您对华为商城的支持。</span>
								<em>2016-07-20 10:43:37</em>
							</div>
						</div>
						<div class="pro-inquire-item clearfix">
							<div class="pro-inquire-user">
								<label>网友：</label><span>雷毅</span><s><i class="icon-vip-level-0"></i></s><em>2016-07-19 15:09:27</em>
							</div>
							<div class="pro-inquire-question">
								<label>咨询内容：</label><span>请问P9拍照专业模式下，可以设置光圈大小吗？</span>
							</div>
							<div class="pro-inquire-answer">
								<label>回复：</label><span>您好，欢迎咨询华为商城。不可以，专业拍照模式下支持手动调节 ISO、曝光补偿、白平衡、对焦模式、快门速度等参数，不支持调节光圈，感谢您对华为商城的支持。</span>
								<em>2016-07-19 15:37:59</em>
							</div>
						</div>
						<div class="pro-inquire-item clearfix">
							<div class="pro-inquire-user">
								<label>网友：</label><span>huafans01187451713</span><s><i class="icon-vip-level-0"></i></s><em>2016-07-19 14:13:38</em>
							</div>
							<div class="pro-inquire-question">
								<label>咨询内容：</label><span>你好，请问买这款手机带充电器吗，充电器是否能做数据线使用，谢谢</span>
							</div>
							<div class="pro-inquire-answer">
								<label>回复：</label><span>您好，欢迎您咨询华为商城。是带充电器的，充电头与数据线可以分开，有数据线使用，感谢您对华为商城的支持。</span>
								<em>2016-07-19 16:28:23</em>
							</div>
						</div>
					</div>
					<div class="pro-inquire-page clearfix">
						<div class="pro-inquire-record">共<em>4103</em>条</div>
						<!-- 20131220-分页-start -->
						<div class="pager" id="all_prd_counsel">
						</div><!-- 20131220-分页-end -->
					</div>
				</div><!-- 20140624-商品详情-全部咨询-end -->

				<!-- 20140624-商品详情-商品咨询-start -->
				<div class="pro-detail-tab-area pro-inquire-area " id="prddetail_counsel_prd" style="display:none;">
					<!-- 20140624-商品详情-咨询提示-start -->
					<div class="pro-inquire-tips">
						<label>温馨提示：</label>因产线可能更改商品包装、产地、附配件等未及时通知，且每位咨询者购买、提问时间等不同。为此，客服给到的回复仅对提问者3天内有效，其他网友仅供参考！给您带来的不便还请谅解，谢谢！
					</div><!-- 20140624-商品详情-咨询提示-end -->
					<div class="pro-inquire-list" id="prd_prd_counsel_content">
					</div>
					<div class="pro-inquire-page clearfix" id="prddetail_counsel_prd_total">
						<div class="pro-inquire-record">共<em></em>条</div>
						<!-- 20131220-分页-start -->
						<div class="pager" id="prd_prd_counsel">
						</div><!-- 20131220-分页-end -->
					</div>
				</div><!-- 20140624-商品详情-商品咨询-end -->

				<!-- 20140624-商品-支付咨询-start -->
				<div class="pro-detail-tab-area pro-inquire-area " id="prddetail_counsel_pay" style="display:none;">
					<!-- 20140624-商品详情-咨询提示-start -->
					<div class="pro-inquire-tips" >
						<label>温馨提示：</label>因产线可能更改商品包装、产地、附配件等未及时通知，且每位咨询者购买、提问时间等不同。为此，客服给到的回复仅对提问者3天内有效，其他网友仅供参考！给您带来的不便还请谅解，谢谢！
					</div><!-- 20140624-商品-咨询提示-end -->
					<div class="pro-inquire-list" id="pay_prd_counsel_content">
					</div>
					<div class="pro-inquire-page clearfix" id="prddetail_counsel_pay_total">
						<div class="pro-inquire-record">共<em></em>条</div>
						<!-- 20131220-分页-start -->
						<div class="pager" id="pay_prd_counsel_page">
						</div><!-- 20131220-分页-end -->
					</div>
				</div><!-- 20140624-商品-支付咨询-end -->

				<!-- 20140624-商品-配送咨询-start -->
				<div class="pro-detail-tab-area pro-inquire-area " id="prddetail_counsel_trans" style="display:none;">
					<!-- 20140624-商品详情-咨询提示-start -->
					<div class="pro-inquire-tips">
						<label>温馨提示：</label>因产线可能更改商品包装、产地、附配件等未及时通知，且每位咨询者购买、提问时间等不同。为此，客服给到的回复仅对提问者3天内有效，其他网友仅供参考！给您带来的不便还请谅解，谢谢！
					</div><!-- 20140624-商品详情-咨询提示-end -->
					<div class="pro-inquire-list" id="trans_prd_counsel_content">
					</div>
					<!--分页判断-->
					<div class="pro-inquire-page clearfix" id="prddetail_counsel_trans_total">
						<div class="pro-inquire-record" >共<em></em>条</div>
						<!-- 20131220-分页-start -->
						<div class="pager" id="trans_prd_counsel_page" >
						</div>
						<!-- 20131220-分页-end -->
					</div>
				</div><!-- 20140624-商品-配送咨询-end -->

				<!-- 20140624-商品-售后咨询-start -->
				<div class="pro-detail-tab-area pro-inquire-area " id="prddetail_counsel_service" style="display:none;">
					<!-- 20140624-商品详情-咨询提示-start -->
					<div class="pro-inquire-tips">
						<label>温馨提示：</label>因产线可能更改商品包装、产地、附配件等未及时通知，且每位咨询者购买、提问时间等不同。为此，客服给到的回复仅对提问者3天内有效，其他网友仅供参考！给您带来的不便还请谅解，谢谢！
					</div><!-- 20140624-商品详情-咨询提示-end -->
					<div class="pro-inquire-list" id="ser_prd_counsel_content">
					</div>
					<div class="pro-inquire-page clearfix" id="prddetail_counsel_serv_total">
						<div class="pro-inquire-record">共<em></em>条</div>
						<!-- 20131220-分页-start -->
						<div class="pager" id="ser_prd_counsel_page">
						</div><!-- 20131220-分页-end -->
					</div>
				</div><!-- 20140624-商品-售后咨询-end -->

				<!-- 20140624-商品-常见问题-start -->
				<div class="pro-detail-tab-area pro-faq-area " id="prddetail_counsel_ques" style="display:none;">
					<!-- 20140624-商品详情-常见问题提示-start -->
					<div class="pro-faq-tips">
						<label>温馨提示：</label> 因商城随时可能更新商品版本、价格、附配件、赠品等信息未及时通知，且每位顾客的购买情况等也不同，为此以下常见问题答复仅供参考，具体请以下单页面显示为准！若给您带来的不便还请谅解，谢谢！
					</div><!-- 20140624-商品详情-咨询提示-end -->
					<div class="pro-faq-list" id="ques_prd_counsel_content">
							<div class="pro-inquire-empty"><p>暂无相关内容</p></div>
					</div>
				</div><!-- 20140624-商品详情-全部咨询-end -->

				<!-- 20140624-商品详情-咨询表单-start -->
				<div id="inquire-form" class="pro-inquire-form-area">
					<div class="h">
						<h3>发表咨询</h3>
						<p>声明：您可在购买前对产品包装、颜色、套餐、运输、支付等方面进行咨询，我们有专人进行回复！因产线可能会更改一些产品的包装、颜色、产地等参数，所以该回复仅在当时对提问者有效，其他网友仅供参考！咨询回复的工作时间为：周一至周五，9:00至18:00，请耐心等待工作人员回复。</p>
					</div>
					<div class="b">
						<div class="form-edit-area">
							<form autocomplete="off"  id="counsel_content_form">
								<div class="form-edit-table">
									<p>
										<b>咨询类型：</b>
										<input type="radio" class="radio vam" value="1" name="type" checked ><label class="vam" for="">商品咨询</label>
										<input type="radio" class="radio vam" value="2" name="type"><label class="vam" for="">支付</label>
										<input type="radio" class="radio vam" value="3" name="type"><label class="vam" for="">配送</label>
										<input type="radio" class="radio vam" value="4" name="type"><label class="vam" for="">售后</label>
									</p>
									<p><textarea placeholder=""  onfocus="ec.product.loginCheckBefCoun();" class="textarea" name="question" id="counseltextid"  maxlength="100"></textarea></p>
									<p style="margin-top: -15px;"><input type="checkbox" id="safeEmail" name="safeEmail" class="checkbox vam" vlaue="1"/><label class="vam">客服回复发送到我的安全邮箱</label></p>
								</div>
								<div class="form-edit-action">
									<input type="button" class="button-style-4 button-inquire-sumbit" value="提交" onclick="ec.product.submitCounsel();">
									&nbsp;&nbsp;<span class="vam error" id="error-span" style="display:none"></span>
								</div>
							</form>
						</div>
					</div>
				</div><!-- 20140624-商品详情-咨询表单-end -->

			</div>
	    </div>
	    <div class="fl u-1-4">
	    <!--购买该商品的用户还购买了 start-->
	    <!--购买该商品的用户还购买了 end-->
	<div class="hot-area">
		<div class="h">
			<h3><span>热销榜单</span></h3>
		</div>
		<div class="b">
			<!--商品列表 -->
			<div class="pro-list">
			<ul>
						<li>
				<div>
				    <p class="p-img"><a href="/product/186517099.html#859934072" title="荣耀V8 4GB+32GB 移动/联通版（铂光金）"><img src="images/60_60_1467187031543mp.jpg" alt="荣耀V8 4GB+32GB 移动/联通版（铂光金）"/></a><s class="s1"></s></p>
				    <p class="p-name"><a href="/product/186517099.html#859934072" title="荣耀V8 4GB+32GB 移动/联通版（铂光金）">荣耀V8 4GB+32GB 移动/联通版（铂光金）</a></p>
				    <p class="p-price"><b>&yen;2299</b></p>
				</div>
			    </li>
				<li>
				<div>
				    <p class="p-img"><a href="/product/2172.html#6146" title="荣耀7 双卡双待双通 移动4G增强版 32GB存储（冰河银）"><img src="images/60_60_1468376124419mp.jpg" alt="荣耀7 双卡双待双通 移动4G增强版 32GB存储（冰河银）"/></a><s class="s2"></s></p>
				    <p class="p-name"><a href="/product/2172.html#6146" title="荣耀7 双卡双待双通 移动4G增强版 32GB存储（冰河银）">荣耀7 移动4G增强版（冰河银）</a></p>
				    <p class="p-price"><b>&yen;1599</b></p>
				</div>
			    </li>
				<li>
				<div>
				    <p class="p-img"><a href="/product/3325.html#8259" title="HUAWEI P9 4GB+64GB 全网通版（陶瓷白）"><img src="images/60_60_1467252608007mp.jpg" alt="HUAWEI P9 4GB+64GB 全网通版（陶瓷白）"/></a><s class="s3"></s></p>
				    <p class="p-name"><a href="/product/3325.html#8259" title="HUAWEI P9 4GB+64GB 全网通版（陶瓷白）">HUAWEI P9 全网通版（陶瓷白）</a></p>
				    <p class="p-price"><b>&yen;3688</b></p>
				</div>
			    </li>
				<li>
				<div>
				    <p class="p-img"><a href="/product/641202729.html#755939196" title="荣耀畅玩5A 双卡双待 移动版 智能手机（白色）"><img src="images/60_60_1468566349056mp.jpg" alt="荣耀畅玩5A 双卡双待 移动版 智能手机（白色）"/></a><s class="s4"></s></p>
				    <p class="p-name"><a href="/product/641202729.html#755939196" title="荣耀畅玩5A 双卡双待 移动版 智能手机（白色）">荣耀畅玩5A 双卡双待 移动版 智能手机（白色）</a></p>
				    <p class="p-price"><b>&yen;699</b></p>
				</div>
			    </li>
				<li>
				<div>
				    <p class="p-img"><a href="/product/2519.html#418934910" title="荣耀畅玩5X 双卡双待 移动版 智能手机（落日金）"><img src="images/60_60_1466583930933mp.jpg" alt="荣耀畅玩5X 双卡双待 移动版 智能手机（落日金）"/></a><s class="s5"></s></p>
				    <p class="p-name"><a href="/product/2519.html#418934910" title="荣耀畅玩5X 双卡双待 移动版 智能手机（落日金）">荣耀畅玩5X 双卡双待 移动版 智能手机（落日金）</a></p>
				    <p class="p-price"><b>&yen;999</b></p>
				</div>
			    </li>
				<li>
				<div>
				    <p class="p-img"><a href="/product/2647.html#6035" title="华为 HUAWEI Mate 8 3GB+32GB版 移动定制（月光银）"><img src="images/60_60_1468899660616mp.jpg" alt="华为 HUAWEI Mate 8 3GB+32GB版 移动定制（月光银）"/></a><s class="s6"></s></p>
				    <p class="p-name"><a href="/product/2647.html#6035" title="华为 HUAWEI Mate 8 3GB+32GB版 移动定制（月光银）">华为 HUAWEI Mate 8（月光银）</a></p>
				    <p class="p-price"><b>&yen;2699</b></p>
				</div>
			    </li>
			</ul>
		    </div>
		</div>
	</div>
	        <div id="pro-seg-hot" class="hr-20"></div>
	<!-- 最近浏览过的商品  -->
	<div id="product-history-area" class="rl-area hide">
	    <div class="h">
	        <h3 class="fl"><span>最近浏览过的商品</span></h3>
	        <span class="fr"><a class="icon-clear" href="javascript:;" onclick="ec.product.history.clear(function(){$('#product-history-area').hide();});">清空</a></span>
	    </div>
	    <div class="b">
	        <!--商品列表  -->
	        <div class="pro-list">
	            <ul id="product-history-list"></ul>
	        </div>
	    </div>
	</div>    </div>
	</div>

	<div class="hr-60"></div>

	<!--到货通知弹出框-->
	<textarea id="product-arrival-html" class="hide">
		<div class="arrival-remind-area">
			<ul class="clearfix">
				<li id="arrival-email">
					<p class="a-title">邮件通知</p>
					<p id="account-email" class="a-txt">xxxxxxxxxxxxxxxxxxxx@huawei.com</p>
					<s></s>
				</li>
				<li id="arrival-mobile">
					<p class="a-title">短信通知</p>
					<p id="account-mobile" class="a-txt">185&nbsp;6644&nbsp;5856</p>
					<s></s>
				</li>		
			</ul>
		</div>
		<div class="box-custom-button">
			<input type="submit" class="box-ok box-button-style-1" value="提交"/>
		</div>
		<div id="arrival-error" class="box-form-tips hide">
			<span class="icon-error">输入有误</span>
		</div>
	</textarea>


	<!-- 20150331-咨询提交成功提示-start -->
	<textarea id="product-counsel-html" class="hide">
	    <div class="box-prompt-success-area">
			<div class="h">
			<i></i>
			</div>
			<div class="b">
				<p>咨询提交成功，请耐心等待客服人员的答复。</p>
				<p id="counsel-success-msg">回复将发送到您的安全邮箱，您还未绑定，请先绑定安全邮箱。</p>
			</div>
		</div>
		<div id="counsel-bind-email" class="box-custom-button">
		    <a href="javascript:;" class="box-change box-button-style-2" target="_blank" onclick="ec.product.secEmailOper()"><span>去绑定</span></a>
		</div>
	</textarea>  	

	<textarea id="product-attention-html" class="hide">
	<!-- 20130423-表单-关注-start -->
	<div class="form-interest-area">
		<div class="form-edit-area">
			<form method="post" onsubmit="return false;">
			<table border="0" cellpadding="0" cellspacing="0">
				<tbody><tr>
					<th>邮箱地址</th>
					<td><input class="text vam span-300" maxlength="50" name="email" value="" type="text"></td>
				</tr>
				<tr>
					<th>手机号码</th>
					<td><input class="text vam span-300" maxlength="12" name="mobile" value="" type="text"></td>
				</tr>
			</tbody></table>
			</form>
		</div>
		<div class="form-edit-action"><input class="button-action-submit-2 box-ok" value="" type="submit"></div>
	</div><!-- 20130423-表单-关注-end -->
	</textarea>                    

	<script src="js/53b49f8901ca4f6b9f92e1afa076ca7b.js"></script>
	<script>
	(function(){
	var hash = location.hash, skuId = 0, virId = 0,
		cateList = [];


	if(hash.length > 1) {
		var skuInfo = hash.split('#', 2)[1];
		skuId = (skuInfo.length > 1) ? (parseInt(skuInfo.split(',', 2)[0], 10) || 0) : 0;
		virId = (skuInfo.length > 1) ? (parseInt(skuInfo.split(',', 2)[1], 10) || 0) : 0;
	} 

	if(virId > 0) {
		$.get('/category.json', {}, function (json) {
			if(!json.success) return;
			var lst = [];
			for (var i = 0; i < json.virtualList.length; i += 1) {
				lst = json.virtualList[i];
				cateList[lst.id] = lst.text;
			}
			$('#bread-pro-name').before(cateList[virId]);
		}, 'json');
		
	}

	ec.context = ""; ec.product.addSkuAttr("8259" , "100102" , "颜色", "80905" , "陶瓷白"); ec.product.addSkuAttr("8259" , "100389" , "制式", "80907" , "全网通版"); ec.product.addSkuAttr("8259" , "103869" , "内存", "80909" , "4GB+64GB"); ec.product.addSkuAttr("8261" , "100102" , "颜色", "80911" , "玫瑰金"); ec.product.addSkuAttr("8261" , "100389" , "制式", "80913" , "全网通版"); ec.product.addSkuAttr("8261" , "103869" , "内存", "80915" , "4GB+64GB"); ec.product.addSkuAttr("8263" , "100102" , "颜色", "80917" , "皓月银"); ec.product.addSkuAttr("8263" , "100389" , "制式", "80919" , "全网通版"); ec.product.addSkuAttr("8263" , "103869" , "内存", "80921" , "3GB+32GB"); ec.product.addSkuAttr("8265" , "100102" , "颜色", "80923" , "钛银灰"); ec.product.addSkuAttr("8265" , "100389" , "制式", "80925" , "全网通版"); ec.product.addSkuAttr("8265" , "103869" , "内存", "80927" , "3GB+32GB"); ec.product.addSkuAttr("8267" , "100102" , "颜色", "80929" , "流光金"); ec.product.addSkuAttr("8267" , "100389" , "制式", "80931" , "全网通版"); ec.product.addSkuAttr("8267" , "103869" , "内存", "80933" , "3GB+32GB"); ec.product.addSkuAttr("8269" , "100102" , "颜色", "80935" , "皓月银"); ec.product.addSkuAttr("8269" , "100389" , "制式", "80937" , "电信定制版"); ec.product.addSkuAttr("8269" , "103869" , "内存", "80939" , "3GB+32GB"); ec.product.addSkuAttr("8273" , "100102" , "颜色", "80947" , "皓月银"); ec.product.addSkuAttr("8273" , "100389" , "制式", "80949" , "移动定制版"); ec.product.addSkuAttr("8273" , "103869" , "内存", "80951" , "3GB+32GB"); ec.product.addSkuAttr("8275" , "100102" , "颜色", "80953" , "钛银灰"); ec.product.addSkuAttr("8275" , "100389" , "制式", "80955" , "移动定制版"); ec.product.addSkuAttr("8275" , "103869" , "内存", "80957" , "3GB+32GB"); ec.product.addSkuAttr("8277" , "100102" , "颜色", "80959" , "皓月银"); ec.product.addSkuAttr("8277" , "100389" , "制式", "80961" , "联通定制版"); ec.product.addSkuAttr("8277" , "103869" , "内存", "80963" , "3GB+32GB"); ec.product.addSkuAttr("8279" , "100102" , "颜色", "80965" , "钛银灰"); ec.product.addSkuAttr("8279" , "100389" , "制式", "80967" , "联通定制版"); ec.product.addSkuAttr("8279" , "103869" , "内存", "80969" , "3GB+32GB"); ec.product.addSkuAttr("721625417" , "100102" , "颜色", "80971" , "琥珀灰"); ec.product.addSkuAttr("721625417" , "100389" , "制式", "80973" , "全网通版"); ec.product.addSkuAttr("721625417" , "103869" , "内存", "80975" , "4GB+64GB"); ec.product.addSkuAttr("8255" , "100102" , "颜色", "83931" , "金色"); ec.product.addSkuAttr("8255" , "100389" , "制式", "83933" , "全网通版"); ec.product.addSkuAttr("8255" , "103869" , "内存", "83935" , "4GB+64GB"); ec.product.setSkuShowType("100102" , "1"); ec.product.setSkuShowType("100389" , "2"); ec.product.setSkuShowType("103869" , "2"); ec.product.id = "3325"; ec.product.defaultSku = "8255"; ec.product.setSkuId = (!ec.product.getSkuInfo(skuId)) ? 0 : skuId; ec.product.status = "2"; ec.product.virId = virId; ec.product.productType = "0"; var _imgName, _giftList, _promotionsList, _prolongLst, _contractList,_groupPhotoList; _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467249557057.jpg");_imgName.push("1467249560122.jpg");_imgName.push("1467249566489.jpg");_imgName.push("1467249569195.jpg");_imgName.push("1467249563584.jpg");_imgName.push("1467249589893.jpg");_imgName.push("1467249595386.jpg");_imgName.push("1467249592572.jpg");_imgName.push("1467249583962.jpg");_imgName.push("1467249586302.jpg");_imgName.push("1467249598629.jpg"); _groupPhotoList.push({name:"1467249557057.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249560122.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249566489.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249569195.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249563584.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249589893.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249595386.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249592572.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249583962.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249586302.jpg", path:"/product/6901443118847/group/"});_groupPhotoList.push({name:"1467249598629.jpg", path:"/product/6901443118847/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8255" , { groupPhotoList:_groupPhotoList, code : "10110010133601", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "3688", originPrice : "3688", name : "HUAWEI P9 4GB+64GB 全网通版（金色）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:06:56", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467252520756mp.jpg", photoPath : "/product/6901443118847/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467252560857.jpg");_imgName.push("1467252564159.jpg");_imgName.push("1467252566788.jpg");_imgName.push("1467252569418.jpg");_imgName.push("1467252572179.jpg");_imgName.push("1467252574763.jpg");_imgName.push("1467252577831.jpg");_imgName.push("1467252580692.jpg");_imgName.push("1467252583538.jpg");_imgName.push("1467252585997.jpg");_imgName.push("1467252589867.jpg");_imgName.push("1467252593129.jpg"); _groupPhotoList.push({name:"1467252560857.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252564159.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252566788.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252569418.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252572179.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252574763.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252577831.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252580692.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252583538.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252585997.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252589867.jpg", path:"/product/6901443118854/group/"});_groupPhotoList.push({name:"1467252593129.jpg", path:"/product/6901443118854/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8259" , { groupPhotoList:_groupPhotoList, code : "10110010133602", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "3688", originPrice : "3688", name : "HUAWEI P9 4GB+64GB 全网通版（陶瓷白）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:06:31", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467252608007mp.jpg", photoPath : "/product/6901443118854/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467252640458.jpg");_imgName.push("1467252643239.jpg");_imgName.push("1467252645630.jpg");_imgName.push("1467252648035.jpg");_imgName.push("1467252650422.jpg");_imgName.push("1467252653405.jpg");_imgName.push("1467252656326.jpg");_imgName.push("1467252658816.jpg");_imgName.push("1467252662176.jpg");_imgName.push("1467252664910.jpg");_imgName.push("1467252668325.jpg");_imgName.push("1467252670887.jpg"); _groupPhotoList.push({name:"1467252640458.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252643239.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252645630.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252648035.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252650422.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252653405.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252656326.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252658816.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252662176.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252664910.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252668325.jpg", path:"/product/6901443118861/group/"});_groupPhotoList.push({name:"1467252670887.jpg", path:"/product/6901443118861/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8261" , { groupPhotoList:_groupPhotoList, code : "10110010133603", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "3688", originPrice : "3688", name : "HUAWEI P9 4GB+64GB 全网通版（玫瑰金）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:06:05", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467252677697mp.jpg", photoPath : "/product/6901443118861/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467252735000.jpg");_imgName.push("1467252738198.jpg");_imgName.push("1467252740889.jpg");_imgName.push("1467252744173.jpg");_imgName.push("1467252746810.jpg");_imgName.push("1467252751780.jpg");_imgName.push("1467252754450.jpg");_imgName.push("1467252756974.jpg");_imgName.push("1467252759912.jpg");_imgName.push("1467252763128.jpg");_imgName.push("1467252767261.jpg"); _groupPhotoList.push({name:"1467252735000.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252738198.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252740889.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252744173.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252746810.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252751780.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252754450.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252756974.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252759912.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252763128.jpg", path:"/product/6901443118878/group/"});_groupPhotoList.push({name:"1467252767261.jpg", path:"/product/6901443118878/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8263" , { groupPhotoList:_groupPhotoList, code : "10110010133604", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "3188", originPrice : "3188", name : "HUAWEI P9 3GB+32GB 全网通版（皓月银）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:05:33", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467252774332mp.jpg", photoPath : "/product/6901443118878/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467252829609.jpg");_imgName.push("1467252832720.jpg");_imgName.push("1467252836308.jpg");_imgName.push("1467252839068.jpg");_imgName.push("1467252841891.jpg");_imgName.push("1467252844655.jpg");_imgName.push("1467252847569.jpg");_imgName.push("1467252850291.jpg");_imgName.push("1467252853054.jpg");_imgName.push("1467252856364.jpg");_imgName.push("1467252859384.jpg"); _groupPhotoList.push({name:"1467252829609.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252832720.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252836308.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252839068.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252841891.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252844655.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252847569.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252850291.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252853054.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252856364.jpg", path:"/product/6901443118885/group/"});_groupPhotoList.push({name:"1467252859384.jpg", path:"/product/6901443118885/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8265" , { groupPhotoList:_groupPhotoList, code : "10110010133605", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "3188", originPrice : "3188", name : "HUAWEI P9 3GB+32GB 全网通版（钛银灰）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:04:49", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467252867876mp.jpg", photoPath : "/product/6901443118885/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467252894532.jpg");_imgName.push("1467252897759.jpg");_imgName.push("1467252900467.jpg");_imgName.push("1467252904027.jpg");_imgName.push("1467252907017.jpg");_imgName.push("1467252910366.jpg");_imgName.push("1467252912931.jpg");_imgName.push("1467252915661.jpg");_imgName.push("1467252918171.jpg");_imgName.push("1467252921579.jpg");_imgName.push("1467252924196.jpg"); _groupPhotoList.push({name:"1467252894532.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252897759.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252900467.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252904027.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252907017.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252910366.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252912931.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252915661.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252918171.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252921579.jpg", path:"/product/6901443118892/group/"});_groupPhotoList.push({name:"1467252924196.jpg", path:"/product/6901443118892/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8267" , { groupPhotoList:_groupPhotoList, code : "10110010133606", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "3188", originPrice : "3188", name : "HUAWEI P9 3GB+32GB 全网通版（流光金）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:04:24", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467252927907mp.jpg", photoPath : "/product/6901443118892/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467252961100.jpg");_imgName.push("1467252964504.jpg");_imgName.push("1467252967167.jpg");_imgName.push("1467252970325.jpg");_imgName.push("1467252973599.jpg");_imgName.push("1467252977393.jpg");_imgName.push("1467252979799.jpg");_imgName.push("1467252982396.jpg");_imgName.push("1467252984957.jpg");_imgName.push("1467252988599.jpg");_imgName.push("1467252991135.jpg"); _groupPhotoList.push({name:"1467252961100.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252964504.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252967167.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252970325.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252973599.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252977393.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252979799.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252982396.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252984957.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252988599.jpg", path:"/product/6901443119707/group/"});_groupPhotoList.push({name:"1467252991135.jpg", path:"/product/6901443119707/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8269" , { groupPhotoList:_groupPhotoList, code : "10110010133607", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "2988", originPrice : "2988", name : "HUAWEI P9 3GB+32GB 电信定制版（皓月银）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:00:54", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467253007932mp.jpg", photoPath : "/product/6901443119707/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:56", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467253169052.jpg");_imgName.push("1467253184233.jpg");_imgName.push("1467253187163.jpg");_imgName.push("1467253190202.jpg");_imgName.push("1467253193581.jpg");_imgName.push("1467253202563.jpg");_imgName.push("1467253205114.jpg");_imgName.push("1467253210467.jpg");_imgName.push("1467253215108.jpg");_imgName.push("1467253220997.jpg");_imgName.push("1467253223403.jpg"); _groupPhotoList.push({name:"1467253169052.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253184233.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253187163.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253190202.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253193581.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253202563.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253205114.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253210467.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253215108.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253220997.jpg", path:"/product/6901443119974/group/"});_groupPhotoList.push({name:"1467253223403.jpg", path:"/product/6901443119974/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8273" , { groupPhotoList:_groupPhotoList, code : "10110010133609", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "2988", originPrice : "2988", name : "HUAWEI P9 3GB+32GB 移动定制版（皓月银）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:00:24", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467253265493mp.jpg", photoPath : "/product/6901443119974/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467253328913.jpg");_imgName.push("1467253336354.jpg");_imgName.push("1467253339121.jpg");_imgName.push("1467253341956.jpg");_imgName.push("1467253387653.jpg");_imgName.push("1467253397213.jpg");_imgName.push("1467253400419.jpg");_imgName.push("1467253404224.jpg");_imgName.push("1467253407083.jpg");_imgName.push("1467253409748.jpg");_imgName.push("1467253412200.jpg"); _groupPhotoList.push({name:"1467253328913.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253336354.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253339121.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253341956.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253387653.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253397213.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253400419.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253404224.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253407083.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253409748.jpg", path:"/product/6901443119981/group/"});_groupPhotoList.push({name:"1467253412200.jpg", path:"/product/6901443119981/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8275" , { groupPhotoList:_groupPhotoList, code : "10110010133610", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "2988", originPrice : "2988", name : "HUAWEI P9 3GB+32GB 移动定制版（钛银灰）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 14:59:55", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467253421815mp.jpg", photoPath : "/product/6901443119981/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467253812592.jpg");_imgName.push("1467253819965.jpg");_imgName.push("1467253823192.jpg");_imgName.push("1467253826456.jpg");_imgName.push("1467253842006.jpg");_imgName.push("1467253834297.jpg");_imgName.push("1467253837533.jpg");_imgName.push("1467253830924.jpg");_imgName.push("1467253815556.jpg");_imgName.push("1467253852307.jpg");_imgName.push("1467253854975.jpg"); _groupPhotoList.push({name:"1467253812592.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253819965.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253823192.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253826456.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253842006.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253834297.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253837533.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253830924.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253815556.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253852307.jpg", path:"/product/6901443119998/group/"});_groupPhotoList.push({name:"1467253854975.jpg", path:"/product/6901443119998/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8277" , { groupPhotoList:_groupPhotoList, code : "10110010133611", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "2988", originPrice : "2988", name : "HUAWEI P9 3GB+32GB 联通定制版（皓月银）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 14:59:17", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467253885714mp.jpg", photoPath : "/product/6901443119998/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467253912355.jpg");_imgName.push("1467254112426.jpg");_imgName.push("1467254115370.jpg");_imgName.push("1467254118314.jpg");_imgName.push("1467254121146.jpg");_imgName.push("1467254123663.jpg");_imgName.push("1467254126238.jpg");_imgName.push("1467254130274.jpg");_imgName.push("1467254132739.jpg");_imgName.push("1467254135595.jpg");_imgName.push("1467254138452.jpg"); _groupPhotoList.push({name:"1467253912355.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254112426.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254115370.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254118314.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254121146.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254123663.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254126238.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254130274.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254132739.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254135595.jpg", path:"/product/6901443120000/group/"});_groupPhotoList.push({name:"1467254138452.jpg", path:"/product/6901443120000/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("8279" , { groupPhotoList:_groupPhotoList, code : "10110010133612", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "2988", originPrice : "2988", name : "HUAWEI P9 3GB+32GB 联通定制版（钛银灰）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 14:58:29", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467254147578mp.jpg", photoPath : "/product/6901443120000/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); _imgName = []; _giftList= []; _promotionsList = []; _prolongLst=[]; _contractList = [];_groupPhotoList=[]; _imgName.push("1467254640367.jpg");_imgName.push("1467254643552.jpg");_imgName.push("1467254646225.jpg");_imgName.push("1467254649179.jpg");_imgName.push("1467254652038.jpg");_imgName.push("1467254655285.jpg");_imgName.push("1467254659027.jpg");_imgName.push("1467254662330.jpg");_imgName.push("1467254666302.jpg");_imgName.push("1467254669099.jpg");_imgName.push("1467254671842.jpg");_imgName.push("1467254674560.jpg"); _groupPhotoList.push({name:"1467254640367.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254643552.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254646225.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254649179.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254652038.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254655285.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254659027.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254662330.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254666302.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254669099.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254671842.jpg", path:"/product/6901443124930/group/"});_groupPhotoList.push({name:"1467254674560.jpg", path:"/product/6901443124930/group/"}); _giftList.push({ giftId : "738132989" , giftName : "华为 HUAWEI Type C 转接头（白色）"}); _prolongLst.push({id:6092,serviceType:1, name:"终端延保服务（一年期）¥188.00",productId:2663});_prolongLst.push({id:820742949,serviceType:1, name:"终端延保服务（半年期）¥98.00",productId:728258707});_prolongLst.push({id:441601818,serviceType:2, name:"碎屏意外保障服务D（一年期）¥139.00",productId:739671590});_prolongLst.push({id:199637048,serviceType:2, name:"意外保障服务D（一年期）¥280.00",productId:472525757}); ec.product.setSku("721625417" , { groupPhotoList:_groupPhotoList, code : "10110010133613", skuPromWord : "精致外观、后置1200万徕卡双摄像头，大光圈拍摄，麒麟955芯片，指纹识别！", skuPromWordLink : "", price : "3688", originPrice : "3688", name : "HUAWEI P9 4GB+64GB 全网通版（琥珀灰）", priceMode : "1", tipsContent : "", buttonMode : "1", gotoUrl : "", startTime : "2016-06-15 15:02:20", countDown : "2", isProm : false, type : "normal", integral : "0", photoName : "1467254679960mp.jpg", photoPath : "/product/6901443124930/", imgName : _imgName, giftList : _giftList, promotionLst : _promotionsList, groupType : 0, groupId : 0, prolongLst : _prolongLst, contractList : _contractList, timerPromWord : "", timerPromLink4PC : "", timerPromLink4WAP : "", timerPromLink4APP : "", timerPromStarttime : "", timerPromEndtime : "", endTime : "2016-06-20 23:59:59", isShowReminder : "0" }); })();</script>
	<script>
	//��֤�Ƿ��п�棨ʵ����Ʒ��
	ec.product.setType("normal");
	ec.track99click({
		cid : "",
		id : "3325"
	});
	</script><script>
	(function () {
		//验证是否有库存（实体商品）
		ec.product.execute("renderInventory", [ec.product.inventory.haveInventory(ec.product.setSkuId || 8255)]);
		
		// 自适配PC和客户端
		var wapUrl = window.location.href;	
		wapUrl = wapUrl.replace("www", "m");
		$("meta[name=mobile-agent]").attr('content', "format=xhtml;url=" + wapUrl);
	})();
	ec.load("ec.pager");
	ec.ready(function(){
		 ec.product.allrenderPage({
			pageNumber : 1,
			totalPage : 821,
			recordCount : 4103
		 });
		 ec.product.prdrenderPage({
			pageNumber : 1
		 });
		 ec.product.payrenderPage({
			pageNumber : 1
			});
		 ec.product.transrenderPage({
			pageNumber : 1
			});
		 ec.product.serrenderPage({
			pageNumber : 1
			});
		 ec.product.quesrenderPage({
			pageNumber :1 
			});
	});
	</script>
@endsection