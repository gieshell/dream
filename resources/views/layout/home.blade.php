<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<script>
	var domainMain = 'http://www.vmall.com';
	var domainWap = 'http://m.vmall.com';
	var domainCart = 'http://cart.vmall.com';
	var domainRemark = 'http://remark.vmall.com';
	var domainShoppingConfig = 'http://addr.vmall.com';
	var imagePath = 'http://res.vmallres.com/20160712/images';
	var domainAccount = 'http://www.vmall.com';
	var isUseAccount = 'false';
	var upBindPhoneAddr = 'http://hwid1.vmall.com:8080/oauth2/userCenter/bindAccount/bindMobileAccount_1.jsp?lang=zh-cn&amp;themeName=cloudTheme&amp;reqClientType=26';
	var dominWapRecycle ='http://mobile.huishoubao.com/?pid=1056';
</script>
<script>
	(function() {
		try {
			cookieGet = function(a) {
				var f = null;
				if (document.cookie && document.cookie != "") {
					var d = document.cookie.split(";");
					for (var c = 0; c < d.length; c++) {
						var b = (d[c] || "").replace(
								/^(\s|\u00A0)+|(\s|\u00A0)+$/g, "");
						if (b.substring(0, a.length + 1) == (a + "=")) {
							var e = function(i) {
								i = i.replace(/\+/g, " ");
								var h = '()<>@,;:\\"/[]?={}';
								for (var g = 0; g < h.length; g++) {
									if (i.indexOf(h.charAt(g)) != -1) {
										if (i.startWith('"')) {
											i = i.substring(1)
										}
										if (i.endWith('"')) {
											i = i.substring(0, i.length - 1)
										}
										break
									}
								}
								return decodeURIComponent(i)
							};
							f = e(b.substring(a.length + 1));
							break
						}
					}
				}
				return f
			};
			var uri = location.href;
			//增加产品详情页判断 
			var prodReg = /\/\d+\.html/;
			var isProDetail = prodReg.test(uri);
			var isHonor = uri.indexOf(".com/honor") >0;
			var isHuawei = uri.indexOf(".com/huawei")>0;
			var isRecyle = uri.indexOf(".com/recycle")>0;
			if (uri == domainMain || uri == (domainMain + "/index.html")
					|| uri == (domainMain + "/") || isProDetail|| isHonor || isHuawei||isRecyle) {
				if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i
						.test(navigator.userAgent)) {
					var redirectWap = cookieGet("redirectWap");

					if (!redirectWap == "1" && !isProDetail) {
						if (isHonor) {
							window.location.href = domainWap + "/honor";
						} else if (isHuawei) {
							window.location.href = domainWap + "/huawei";
						} else if (isRecyle) {
							window.location.href = dominWapRecycle;
						} else {
							window.location.href = domainWap;
						}
					} else if (!redirectWap == "1" && isProDetail) {
						var wapUri = domainWap
								+ uri.substring(uri.indexOf("/product"));
						window.location.href = wapUri;
					}
				}
			}
		} catch (err) {
		}
	})();
</script>
<title>【【华为P9】华为 P9【报价 参数 功能 性能 图片 怎么样】】_华为商城</title>
<meta name="keywords" content="华为商城,华为P9,华为 P9,华为,9,华为P9" />
<meta name="description" content="华为P9怎么样？华为商城华为P9震撼首发！双摄像头智能手机，3D指纹识别技术" />
<link rel="shortcut icon" href="http://www.vmall.com/favicon.ico" />
<link href="/homePublic/css/ec.core.min.css" rel="stylesheet" type="text/css">

<link href="/homePublic/css/main.min.css" rel="stylesheet" type="text/css">
<script src="/homePublic/jsjsapi.js" namespace="ec"></script>
<script src="/homePublic/jsjquery-1.4.4.min.js"></script>
<script src="/homePublic/jsec.core.js"></script> 
<script src="/homePublic/jsec.business.js"></script> 
<!--[if lt IE 9]><script src="/homePublic/jshtml5shiv.js"></script> <![endif]-->
</head>



<body>
<div class="top-banner" id="top-banner-block"></div>
<!-- 20130605-qq-彩贝-start -->
<div class="qq-caibei-bar hide" id="caibeiMsg">
	<div class="layout">
		<div class="qq-caibei-bar-tips" id="HeadShow"></div>
		<div class="qq-caibei-bar-userInfo" id="ShowMsg"></div>
	</div>
</div>
<!-- 20130605-qq-彩贝-end -->

<div class="shortcut">
    <div class="layout">
		<div class="s-sub">
			<ul>
				<li class="s-hw"><a href="http://consumer.huawei.com/cn/" target="_blank">华为官网</a></li>
				<li class="s-honor"><a href="http://www.honor.cn/" target="_blank">荣耀官网</a></li>
				<!--
				<li class="s-emui"><a href="http://emui.huawei.com/cn/" target="_blank">EMUI</a></li>
				<li class="s-appstore"><a href="http://appstore.huawei.com/" target="_blank">应用市场</a></li>
				<li class="s-cloud"><a href="http://cloud.huawei.com/cn/contact" target="_blank">云服务</a></li>
				<li class="s-developer"><a href="http://developer.huawei.com/" target="_blank">开发者联盟</a></li>
				-->
				<li class="s-appsoft">
					<div class="s-dropdown">
						<div class="h">
							<a href="http://emui.huawei.com/appsoft/" target="_blank" >软件应用</a>
							<s></s>
						</div>
						<div class="b">
							<p><a href="http://emui.huawei.com/cn/" target="_blank">EMUI</a></p>
							<p><a href="http://appstore.huawei.com/" target="_blank">应用市场</a></p>
							<p><a href="http://cloud.huawei.com" target="_blank">云服务</a></p>
							<p><a href="http://developer.huawei.com/" target="_blank">开发者联盟</a></p>
						</div>
					</div>	
					
				<li class="s-club"><a href="http://club.huawei.com" target="_blank">花粉俱乐部</a></li>
				<li class="s-sr"><a href="javascript:;" onclick="showSelectRegion()">Select Region</a></li>
			</ul>
		</div>
		<div class="s-main">
			<ul>
				<li class="s-login" id="unlogin_status">
						<script>document.write('<a href="https://hwid1.vmall.com/casserver/remoteLogin?loginChannel=26000000&reqClientType=26&loginUrl=http%3A%2F%2Fhwid1.vmall.com%3A8080%2Foauth2%2Fportal%2Flogin.jsp&service=http%3A%2F%2Fwww.vmall.com%2Faccount%2Facaslogin%3Furl%3D'+encodeURIComponent(encodeURIComponent(window.location.href))+'" rel="nofollow">登录</a>');</script>
						&nbsp;&nbsp;&nbsp;<a href="https://hwid1.vmall.com/oauth2/portal/regbymail.jsp?service=http://www.vmall.com/account/caslogin&loginChannel=26000000&reqClientType=26" rel="nofollow">注册</a>
				</li>
				<li class="s-user hide" id="login_status">
					<!--
						ie6下鼠标悬停追加ClassName： hover
						示例：[ s-dropdown hover ]
					-->
					<div class="s-dropdown">
						<div class="h">
							<a href="http://www.vmall.com/member?t=1468985819635timestamp" id="customer_name" rel="nofollow" timeType="timestamp" class="link-user"></a>
							 <em class="vip-state" id="vip-info">
								<a class="link-noAct" href="http://www.vmall.com/member/account" id="vip-inActive" title="请完善个人信息，即刻享受会员特权">去激活</a>
								<a href="http://www.vmall.com/member/point" title="V0" id="vip-Active" ><i class="icon-vip-level-0"></i>&nbsp;</a>
								<a title="实名认证" id="authentication" href="/authmember/accesstoken"></a>
							</em>
							<s></s>
						</div>
						<div class="b">
							<p><a href="https://hwid1.vmall.com/oauth2/userCenter/hwAccount?reqClientType=26&loginChannel=26000000&themeName=cloudTheme" target="_blank" id="user-center">我的华为帐号</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="http://www.vmall.com/account/logout">退出</a></p>
						</div>
					</div>
				</li>
				<li class="s-myOrders">
					<a href="http://www.vmall.com/member/order?t=1468985819635timestamp" rel="nofollow" timeType="timestamp">我的订单</a>
				</li>
				<li class="s-promo"><a href="http://www.vmall.com/priority" rel="nofollow">V码(优购码)</a></li>
				<li class="s-hwep hide" id="preferential"></li>
				<li class="s-mobile"><a href="http://www.vmall.com/appdownload" target="_blank">手机版</a></li>
				<li class="s-sitemap">
					<div class="s-dropdown ">
						<div class="h">
							<a href="javascript:;">网站导航</a>
							<s></s>
						</div>
						<div class="b">
							<p><a href="http://www.vmall.com/help/index.html">帮助中心</a></p>
							<p><a href="http://consumer.huawei.com/minisite/HiSuite_cn/" target="_blank">PC软件</a></p>
							<p><a href="http://vmall.118100.cn" target="_blank">数字音乐</a></p>
							<p><a href="http://www.hwtrip.com/" target="_blank">爱旅</a></p>
							<p><a href="http://dbank.vmall.com/" target="_blank">华为网盘</a></p>
						</div>
					</div>
				</li>
			</ul>
		</div>
    </div>
</div>

<textarea id="selectRegion-tips" style="display: none;">
<!-- 20151105-全球语言弹出框-width:910px-start -->
<div class="box-content"><div class="box-lan-choose"><dl><dt>Asia Pacific</dt><dd class="box-button"><a class="box-choose" href="http://www.vmall.hk/">香港</a><a class="box-choose" href="http://www.vmall.my/">Malaysia</a></dd></dl><dl class="box-lan-choose-area"><dt>The United States and Latin America</dt><dd class="box-button"><a class="box-choose" href="https://store.hihonor.com/us">United States</a><a class="box-choose" href="http://www.honor.mx/">México</a></dd></dl><dl class="box-lan-choose-area"><dt>Europe</dt><dd class="box-button"><a class="box-choose" href="http://www.vmall.ru/">Россия</a><a class="box-choose" href="https://www.vmall.eu/be/">België</a><a class="box-choose" href="https://www.vmall.eu/fr/">France</a><a class="box-choose" href="https://www.vmall.eu/de/">Deutschland</a><a class="box-choose" href="https://www.vmall.eu/it/">Italia</a><a class="box-choose" href="https://www.vmall.eu/nl/">Nederland</a><a class="box-choose" href="https://www.vmall.eu/pt/">Portugal</a><a class="box-choose" href="https://www.vmall.eu/es/">España</a><a class="box-choose" href="https://www.vmall.eu/ch/">Schweiz</a><a class="box-choose" href="https://www.vmall.eu/uk/">UK</a></dd></dl><dl class="box-lan-choose-area"><dt>Middle East</dt><dd class="box-button"><a href="http://www.vmall.hk/" target="_self" textvalue="Saudi Arabia"></a><a class="box-choose" href="http://www.honorarabia.com/uae/">United Arab Emirates</a><a class="box-choose" href="http://www.honorarabia.com/ksa/">Saudi Arabia</a></dd></dl></div></div>
<!-- 20151105-全球语言弹出框-width:910px-end -->					
</textarea>
<script src="/homePublic/jsbase.min.js"></script>
<script>
ec.mediaPath = "http://res.vmallres.com/pimages/";
//document.write('<script src="/homePublic/jsf98f6bbb478a4c9da58023d6e11ac368.js'+(new Date()).getTime()+'"></s' + 'cript>');
</script>
<script>
ec.load("ec.box" , {loadType : "lazy"});
/**
*功能：给鼠标移动到s-dropdown上面的时候，记得给他多添加一个hover类样式
*目的是兼容ie6
*@author 李峰
*/
$(function(){
	$(".s-dropdown").hover(function(){
		$(this).addClass("hover");
	},function(){
		$(this).removeClass("hover");
	});
	var maxcolor=$('.top-banner-max').find('p').children('a').css("background-color");
	var mincolor=$('.top-banner-min').find('p').children('a').css("background-color");
	
	$('.top-banner-max').css({"background-color":maxcolor,overflow:"hidden"});
	$('.top-banner-min').css({"background-color":mincolor,overflow:"hidden"});
});

//显示全球导航选择层
function showSelectRegion()
{
    var box = new ec.box($("#selectRegion-tips").val(),{
			boxid : "region-select-box",
			boxclass : "ol_box_4",
			title : "Please select your country or region.",
			width : 940,			
			showButton : false,
			autoPosition:false,
			onopen: function(box){
			},
			onok : function(box){
			},
			oncancel : function(box){
				box.close();
				$(".ol_box_mask").remove();
			},   
			onclose : function(box){
				$(".ol_box_mask").remove();
			}
		});	
								
		box.open();
				
				$(".box-title").css("font-size","28px").css("font-weight","normal");
				$("#region-select-box").css("height","auto");				
				$(".ol_box_mask").click(function(){box.close();});
				
				$(".box-header").unbind("mousemove");
				$(".box-header").unbind("mousedown");
				
				var divTop=document.getElementById("region-select-box").offsetTop;
				$("#region-select-box").mousedown(function (e) {
					var e=e||window.event;
					var region = document.getElementById("region-select-box");
										
					var leftX = e.clientX - region.offsetLeft;
					var topY = e.clientY - region.offsetTop;
					$("#region-select-box").mousemove(function (event) {
					 var e=event||window.event;
					  var left=	e.clientX - leftX;
					  var top=e.clientY - topY;
					   
					  if(e.clientX - leftX<20-region.offsetWidth){
					  left=20-region.offsetWidth;
					  }
					  if(e.clientY - topY<20-region.offsetHeight){
					  top=20-region.offsetHeight;
					  }	
					 
					 if(e.clientY - topY+20>=$(window).height()){
					  top=$(window).height() - 20;
					  }
					  if(e.clientX - leftX+20>=$(window).width()){
					  left=$(window).width() - 20;
					  }
					  $("#region-select-box").css({"left": left, "top": top});
					  divTop=top;
						
					});
				});
				$("#region-select-box").mouseup(function () {
				$("#region-select-box").unbind("mousemove");
				});
            $(window).scroll(function () {              
                $("#region-select-box").offset({ top: divTop });
            });	
}

</script>
<header class="header">
	<div class="layout">
		<!-- 21030909-logo-start -->
		<div class="logo logo-index"><a href="/index.html" title="Vmall.com - 华为商城"><img src="/homePublic/images/newlogo.png" alt="Vmall.com - 华为商城"/></a></div><!-- 21030909-logo-start -->
		<!-- 20130909-搜索条-焦点为search-form增加className:hover -start -->
		<div class="searchBar">
			<!-- 页头热门搜索 -->
			<div class="searchBar-form" id="searchBar-area">
				<form method="get" onsubmit="return search(this)">					
					<input type="text" class="text" maxlength="100" id="search-kw" /><input type="submit" class="button" value="搜索" />
					<input type="hidden" id="default-search" value="荣耀8"/>
				</form>
			</div>
			
				<div class="searchBar-key">
	<b>热门搜索：</b>
	    
		<a href="/search?keyword=V8" target="_blank">V8</a>
	    
	    
		<a href="/search?keyword=5A" target="_blank">5A</a>
	    
	    
		<a href="/search?keyword=%E8%8D%A3%E8%80%807" target="_blank">荣耀7</a>
	    
	    
		<a href="/search?keyword=5X" target="_blank">5X</a>
	    
	    
		<a href="/search?keyword=HUAWEI%20P9" target="_blank">HUAWEI P9</a>
	    
	    
		<a href="/search?keyword=5C" target="_blank">5C</a>
	    
	    
		<a href="/search?keyword=%E5%BF%AB%E5%85%85" target="_blank">快充</a>
	    
</div>
			
		</div><!-- 20130909-搜索条-end -->
		<!-- 21030910-头部-工具栏-start -->
		<div class="header-toolbar">
			<!-- 21030910-头部-工具栏-焦点为header-toolbar-item增加ClassName:hover -->
			<div class="header-toolbar-item" id="header-toolbar-imall">
				<!-- 21030909-我的商城-start -->
				<div class="i-mall">
					<div class="h"><a href="/member?t=1468985819635timestamp" rel="nofollow" timeType="timestamp">我的商城</a>
					<i></i><s></s><u></u></div>
					<div class="b" id="header-toolbar-imall-content">
						<div class="i-mall-prompt" id="cart_unlogin_info">
							<p>你好，请&nbsp;&nbsp;<script>document.write('<a href="/account/login?url='+encodeURIComponent(window.location.pathname)+'"  rel="nofollow">登录</a>');</script> | <a href="/account/register" rel="nofollow">注册</a></p>
						</div>
						<div class="i-mall-uc " id="cart_login_info">
							<ul>
								<li><a href="http://www.vmall.com/member/order?t=1468985819635timestamp" rel="nofollow" timeType="timestamp">我的订单</a></li>
								<li><a href="/member/order?t=1468985819635timestamp&tab=unpaid" timeType="timestamp">待支付</a><span id="toolbar-orderWaitingHandleCount" class="hide">0</span></li>
								<li><a href="/member/order?t=1468985819635timestamp&tab=nocomment" timeType="timestamp">待评论</a><span id="toolbar-notRemarkCount" class="hide">0</span></li>
								<li><a href="/member/coupon?t=1468985819635timestamp" timeType="timestamp">优惠券</a><span id="toolbar-couponCount" class="hide">0</span></li>
								<li><a href="/member/msg?t=1468985819635timestamp" timeType="timestamp">站内信</a><span id="toolbar-newMsgCount" class="hide">0</span></li>
							</ul>
						</div>
						<!-- 页头会员专享信息 -->
						<div class="i-mall-event " >
							<p><a href="http://www.vmall.com/notice-657"><img src="/homePublic/images/20160127095348354.jpg" alt="特权频道" /></a></p>
						</div>
					</div>
				</div><!-- 21030909-我的商城-end -->
			</div>
			<div class="header-toolbar-item" id="header-toolbar-minicart">
				<!-- 21030909-迷你购物车-start -->
				<div class="minicart">
					<div class="h" id="header-toolbar-minicart-h"><a href="http://cart.vmall.com/cart/cart.html?t=1468985819635timestamp" rel="nofollow" timeType="timestamp">我的购物车<span><em id="header-cart-total">0</em><b></b></span></a><i></i><s></s><u></u></div>
					<div class="b" id="header-toolbar-minicart-content">
						<div class="minicart-pro-empty" id="minicart-pro-empty">
							<span class="icon-minicart">您的购物车是空的，赶紧选购吧！</span>
						</div>
						<div id="minicart-pro-list-block">
						<ul class="minicart-pro-list" id="minicart-pro-list">
														
						</ul>
						</div>
						<div class="minicart-pro-settleup" id="minicart-pro-settleup">
							<p>共<em id="micro-cart-total">0</em>件商品，金额合计<b id="micro-cart-totalPrice">&yen;&nbsp;0</b></p>
							<a class="button-minicart-settleup" href="http://cart.vmall.com/cart/cart.html">去结算</a>
						</div>
					</div>
				</div><!-- 21030909-迷你购物车-end -->
			</div>
			
		</div><!-- 21030910-头部-工具栏-start -->
		<!-- 20140910-头部-二维码-start -->
		<div class="header-qrcode">
			<div class="ec-slider" id="ec-erweima">
				<ul class="ec-slider-list">
					<li class="ec-slider-item">
						<p><a href="http://www.vmall.com/appdownload" target="blank" title="专享周三荣耀专场"><img src="/homePublic/images/qrcode_vmall_app01.png" alt="华为商城官方客户端"/></a></p>
						<p><a href="http://www.vmall.com/appdownload" target="blank"><span>专享周三荣耀专场</span></a></p>
					</li>
					<li class="ec-slider-item">
						<p><a href="http://www.vmall.com/appdownload" target="blank" title="微信扫码关注我们"><img src="/homePublic/images/qrcode_vmall_wechat01.jpg" alt="华为商城官方微信"/></a></p>
						<p><a href="http://www.vmall.com/appdownload" target="blank"><span>微信扫码关注我们</span></a></p>
					</li>
				</ul>
			</div>
		</div><!-- 20140910-头部-二维码-end -->
	</div>			
</header><!-- 21030909-头部-end -->


<textarea id="micro-cart-tpl" class="hide">
<!--#macro microCartList data-->
	
	<!--#list data.bundlerList as b-->
		<!--#if (b.skuList && b.skuList.length > 0) -->
		<!--#var lst = b.skuList[0];-->
		<!--#var skuId='#'+lst.skuId;-->
		<li class="minicart-pro-item">
			<div class="pro-info">
				<div class="p-img"><a href="/product/{#lst.id}.html{#skuId}" title="" target="_blank"><img src="/homePublic/images/{#lst.photopath}78_78_{#lst.photoname}" alt="{#lst.prdSkuName}" /></a></div>
				<div class="p-name"><a href="/product/{#lst.id}.html{#skuId}" title="{#lst.prdSkuName}" target="_blank">{#lst.prdSkuName}&nbsp;<span class="p-slogan">{#lst.skuPromWord}</span><span class="p-promotions hide"></span></a></div>
				<div class="p-status">
					<div class="p-price"><b>&yen;&nbsp;{#parseFloat(lst.skuPrice).toFixed(2)}</b><em>x</em><span>{#b.quantity*lst.quantity}</span></div>
					<div class="p-tags">
						<span class="p-mini-tag-suit">套装</span>
						<span class="p-mini-tag-sale">惠</span>
						<!--#if (lst.giftList && lst.giftList.length > 0)--><span class="p-mini-tag-gift">配</span><!--/#if-->
						<!--#if (lst.extendList && lst.extendList.length > 0)--><span class="p-mini-tag-extend">延保</span><!--/#if-->
					</div>
				</div>
				<a href="javascript:;" class="icon-minicart-del" title="删除" onclick="ec.minicart.del(this , {#b.bundleId}, {#b.productType})">删除</a>
			</div>
		</li>
		<!--/#if-->
	<!--/#list-->
	<!--#list data.productList as lst-->
	<!--#var skuId='#'+lst.skuId;-->
	<li class="minicart-pro-item">
		<div class="pro-info">
			<div class="p-img"><a href="/product/{#lst.id}.html{#skuId}" title="" target="_blank"><img src="/homePublic/images/{#lst.photopath}78_78_{#lst.photoname}" alt="{#lst.prdSkuName}" /></a></div>
			<div class="p-name"><a href="/product/{#lst.id}.html{#skuId}" title="{#lst.prdSkuName}" target="_blank">{#lst.prdSkuName}&nbsp;<span class="p-slogan">{#lst.skuPromWord}</span><span class="p-promotions hide"></span></a></div>
			<div class="p-status">
				<div class="p-price"><b>&yen;&nbsp;{#parseFloat(lst.skuPrice).toFixed(2)}</b><em>x</em><span>{#lst.quantity}</span></div>
				<div class="p-tags">
					<!--#if (lst.campaignInfoList && lst.campaignInfoList.length > 0)--><span class="p-mini-tag-sale">惠</span><!--/#if-->
					<!--#if (lst.giftList && lst.giftList.length > 0)--><span class="p-mini-tag-gift">配</span><!--/#if-->
					<!--#if (lst.extendList && lst.extendList.length > 0)--><span class="p-mini-tag-extend">延保</span><!--/#if-->
				</div>
			</div>
			<a href="javascript:;" class="icon-minicart-del" title="删除" onclick="ec.minicart.del(this , {#lst.skuId}, {#lst.productType})">删除</a>
		</div>
	</li>
	<!--/#list-->
<!--/#macro-->
</textarea>

<script>
ec.load("ec.slider", {
   loadType : "lazy", 
   callback : function() {
		$("#ec-erweima").slider({
				 width:     91, //必须
				 height:     96, //必须
				 style : 1,                    //1显示分页，2只显示左右箭头,3两者都显示
				 pause : 120000,           //间隔时间
				 auto : true

		});
   }
});
ec.ready(function(){
	ec.form.input.label("#search-kw" , "荣耀8");
	ec.ui.hover("#searchBar-area" , { captureInput : true });
});
ec.code.addService({showService : true , showTools : true});

ec.ready(function () {
	ec.cart.getTotal();
	//微型购物车hover效果
	ec.minicart.microCartTpl = new ec.template($('#micro-cart-tpl').val());
	var $imall = $('#header-toolbar-imall-content'),
		$miniCart = $('#header-toolbar-minicart-content');
	$('#header-toolbar-imall').hover(function (){
		$.ajax({
			url : "http://remark.vmall.com/remark/queryNotRemarkCount.json?t=" + new Date().getTime(),
			dataType:"jsonp",
			success : function(json){
				$('#toolbar-notRemarkCount').html(+json.notRemarkCount).attr('class',(json.notRemarkCount <= 0) ? 'hide' : '');
				$('#member-notRemarkCount').text(json.notRemarkCount);
			}
		});
		//
		ec.cart.getTotal();
		
		$imall.show();
	}, function () {
		$imall.hide();
	});
	$('#header-toolbar-minicart').hover(function (){
		ec.cart.getTotal();
		$miniCart.show();
	}, function () {
		$miniCart.hide();
	});
});

$("#getPetal").click(function()
{
	window.location.href="/member/petal";
});

ec.ready(function(){
	try
	{	
			new ec.ajax().get({		
			url:"/account/getPetalValid.json",
			successFunction:function(json){		
				//如果没有签到
				if(!json.success)
				{
					//判断用户是否已签到
					new ec.ajax().get({		
						url:"/account/signinValid.json",
						successFunction:function(json){		
							//如果没有签到
							if(!json.success)
							{
								$("#dfn").show();
							}
							else//如果已经签到
							{
								$("#dfn").hide();
							}
					},
						errorFunction : function(json) {
							// alert(json);
						}
					});
				
				}
				else
				{
					$("#dfn").hide();
				}
			},
			errorFunction : function(json) {
				//alert(json);
			}
		});
		
	}
	catch (err)
	{
		// console.log(err);
	}
});


</script>
<textarea class="hide" id="ec-binding-phone">
	<div id="ec-binding-phone-1" class="ec-binding-phone-box hide">
		<!-- 20141011-绑定安全手机号提示-start -->
		<div class="safetyPhone-prompt-area">
			<div class="h">
				<i></i>
			</div>
			<div class="b">
				<p>为了给您提供更好的服务，建议您将登录的邮箱帐号进行手机号码绑定，绑定后邮箱帐号和绑定的手机号码都可以作为登录帐号，<em style="font-color:red;">不绑定将不能提交订单。</em></p>
			</div>
		</div>
		<div class="box-custom-button">
			<a class="box-button-style-3" href="javascript:;" id="ec-binding-phone-url-1"><span>立即绑定</span></a></a>
		</div>
		<!-- 20141011-绑定安全手机号提示-end -->
	</div>
	<div id="ec-binding-phone-2" class="ec-binding-phone-box hide">
		<!-- 20141424-绑定安全手机号提示-start -->
		<div class="safetyPhone-prompt-area">
			<div class="h">
				<i></i>
			</div>
			<div class="b">
				<p>请您在新打开的页面中完成绑定安全手机号操作。</p>
				<p>完成绑定后请根据您的情况点击下面的按钮。</p>
			</div>
		</div>
		<div class="box-custom-button">
			<a class="box-button-style-3" href="javascript:;" onclick="ec.binding.resetShow()"><span>绑定成功</span></a><a class="box-change box-button-style-3" href="javascript:;" id="ec-binding-phone-url-2"><span>重新绑定</span></a>
		</div>
		<!-- 20141424-绑定安全手机号提示-end -->
	</div>
	<div id="ec-binding-phone-3" class="ec-binding-phone-box hide">
		<!-- 20141424-安全手机号绑定提示失败-start -->
		<div class="box-prompt-error-area">
			<div class="h">
				<i></i>
			</div>
			<div class="b">
				<p class="tal f12">对不起，您未成功绑定手机号。</p>
				<p class="tal f12 black">为了您在商城正常购物、保护您的权益，请您绑定一个手机号码以享受华为商城的所有服务。</p>
			</div>
		</div>
		<div class="box-custom-button">
			<a href="javascript:;" class="box-button-style-3" onclick="ec.binding.showOk()" id="ec-binding-phone-url-3" ><span>立即绑定</span></a>
		</div>
		<!-- 20141424-安全手机号绑定提示失败-end -->
	</div>
	<div id="ec-binding-phone-4" class="ec-binding-phone-box hide">
		<!-- 20150824-手机号绑定未绑定-start -->
		<div class="safetyPhone-prompt-area">
			<div class="h">
				<i></i>
			</div>
			<div class="b">
				<p class="tal f12">为了给您提供更好的服务，建议您将登录的邮箱帐号进行手机号码绑定，绑定后邮箱帐号和绑定的手机号码都可以作为登录帐号；<em style="font-color:red;">自</em><em id="bindEndDate4State4" style="font-color:red;"></em><em style="font-color:red;">号起，若不绑定将不能提交订单。</em></p>
			</div>
		</div>
		<div>&nbsp;</div>
		<div class="box-custom-button">
			<a class="box-change box-button-style-3" href="javascript:;" id="ec-binding-phone-url-4"><span>立即绑定</span></a>
			<a class="box-button-style-1" href="javascript:;" onclick="ec.binding.closeState4();"><span>下次再说</span></a>
		</div>
		<!-- 20150824-安全手机号绑定提示失败-end -->
	</div>
	<div id="ec-binding-phone-5" class="ec-binding-phone-box hide">
		<!-- 20141424-安全手机号绑定提示失败-start -->
		<div class="box-prompt-error-area">
			<div class="h">
				<i></i>
			</div>
			<div class="a">
				<p class="tal f12">您输入的手机已被注册，您可以选择使用手机号重新登录或者重新绑定手机！</p>
			</div>
		</div>
		<div>&nbsp;</div>
		<div class="box-custom-button">
			<a class="box-button-style-3" href="http://www.vmall.com/account/login" id="ec-binding-phone-reLogin-5"><span>重新登录</span></a>')
			<a class="box-change box-button-style-3" href="javascript:;" id="ec-binding-phone-url-5"><span>重新绑定</span></a>
		</div>
		<!-- 20141424-安全手机号绑定提示失败-end -->
	</div>
</textarea><!-- 导航 -->
<div class="naver-main">
	<div class="layout">
		<!-- 20140823-分类-start -->
		<div class="category" id="category-block">
			<!-- 20140822-分类-start -->
	<div class="h">
		<h2>全部商品</h2>
		<i class="icon-category"></i>
	</div>
	<div class="b">
		<ol class="category-list">
			<!-- 鼠标悬停增加ClassName： hover -->
							<li class="category-item ">
					<div class="category-info">
						<h3>
							<a href="/list-36" target="_blank"><span>手机
													</span></a></h3>
								<a href="/list-75" target="_blank"><span>荣耀
																</span></a>
								<a href="/list-77" target="_blank"><span>荣耀畅玩
																</span></a>
								<a href="/list-76" target="_blank"><span>华为 Mate/P系列
																</span></a>
					</div>
					
						
						
								<div class="category-panels children-col-1">
												<ul class="subcate-list children-col-list">
											<li class="subcate-item">
												<a href="/list-75" target="_blank">
													<span>荣耀
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-77" target="_blank">
													<span>荣耀畅玩
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-76" target="_blank">
													<span>华为 Mate/P系列
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-81" target="_blank">
													<span>华为 G/Y系列
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-38" target="_blank">
													<span>运营商合约
																											</span>
												</a>
											</li>
								<dl class="category-banner children-col-2">
									<dt><span>推荐商品</span></dt>
											<dd><a href="http://www.vmall.com/recycle?url=%3Fpid%3D1032" target="_blank">以旧换新
																							</a></dd>
											<dd><a href="http://www.vmall.com/product/2647.html" target="_blank">华为 Mate 8
																							</a></dd>
											<dd><a href="http://www.vmall.com/product/2519.html" target="_blank">荣耀畅玩5X
																							</a></dd>
								</dl>
								</ul>
						</div>
				</li>
				<li class="category-item odd">
					<div class="category-info">
						<h3>
							<a href="/list-40" target="_blank"><span>平板 &amp; 穿戴
													</span></a></h3>
								<a href="/list-41" target="_blank"><span>平板电脑
																</span></a>
								<a href="/list-42" target="_blank"><span>手环
																</span></a>
								<a href="/list-85" target="_blank"><span>手表
																</span></a>
					</div>
					
						
						
								<div class="category-panels children-col-1">
												<ul class="subcate-list children-col-list">
											<li class="subcate-item">
												<a href="/list-41" target="_blank">
													<span>平板电脑
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-42" target="_blank">
													<span>手环
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-85" target="_blank">
													<span>手表
																											</span>
												</a>
											</li>
								<dl class="category-banner children-col-2">
									<dt><span>推荐商品</span></dt>
											<dd><a href="http://www.vmall.com/product/1350.html" target="_blank">荣耀平板
																							</a></dd>
											<dd><a href="http://www.vmall.com/product/2642.html" target="_blank">HUAWEI WATCH
																							</a></dd>
											<dd><a href="http://www.vmall.com/product/2323.html#4989,42" target="_blank">荣耀手环zero
																							</a></dd>
								</dl>
								</ul>
						</div>
				</li>
				<li class="category-item ">
					<div class="category-info">
						<h3>
							<a href="/list-59" ><span>笔记本电脑
													</span></a></h3>
								<a href="/list-241" target="_blank"><span>MateBook
																</span></a>
								<a href="/list-247" target="_blank"><span>拓展坞
																</span></a>
								<a href="/list-249" target="_blank"><span>触控笔
																</span></a>
					</div>
					
						
						
								<div class="category-panels children-col-1">
												<ul class="subcate-list children-col-list">
											<li class="subcate-item">
												<a href="/list-241" target="_blank">
													<span>MateBook
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-247" target="_blank">
													<span>拓展坞
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-249" target="_blank">
													<span>触控笔
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-251" target="_blank">
													<span>二合一键盘
																											</span>
												</a>
											</li>
						</div>
				</li>
				<li class="category-item odd">
					<div class="category-info">
						<h3>
							<a href="/list-43" ><span>智能家居
													</span></a></h3>
								<a href="/list-263" ><span>子母路由
																</span></a>
								<a href="/list-44" target="_blank"><span>电力猫
																</span></a>
								<a href="/list-45" target="_blank"><span>路由器
																</span></a>
					</div>
					
						
						
								<div class="category-panels children-col-1">
												<ul class="subcate-list children-col-list">
											<li class="subcate-item">
												<a href="/list-263" >
													<span>子母路由
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-44" target="_blank">
													<span>电力猫
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-45" target="_blank">
													<span>路由器
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-46" target="_blank">
													<span>电视盒子
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-97" >
													<span>随行wifi
																											</span>
												</a>
											</li>
								<dl class="category-banner children-col-2">
									<dt><span>推荐商品</span></dt>
											<dd><a href="http://www.vmall.com/product/1822.html#3463" target="_blank">荣耀路由
																							</a></dd>
											<dd><a href="http://www.vmall.com/product/1680.html#3102,46" target="_blank">荣耀盒子
																							</a></dd>
								</dl>
								</ul>
						</div>
				</li>
				<li class="category-item ">
					<div class="category-info">
						<h3>
							<a href="/list-47" target="_blank"><span>专属配件
													</span></a></h3>
								<a href="/list-48" target="_blank"><span>保护壳
																</span></a>
								<a href="/list-49" target="_blank"><span>保护套
																</span></a>
								<a href="/list-50" target="_blank"><span>贴膜
																</span></a>
					</div>
					
						
						
								<div class="category-panels children-col-1">
												<ul class="subcate-list children-col-list">
											<li class="subcate-item">
												<a href="/list-48" target="_blank">
													<span>保护壳
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-49" target="_blank">
													<span>保护套
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-50" target="_blank">
													<span>贴膜
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-223" >
													<span>盒子遥控器
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-225" target="_blank">
													<span>表带
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-227" target="_blank">
													<span>电容笔
																											</span>
												</a>
											</li>
								<dl class="category-banner children-col-2">
									<dt><span>推荐商品</span></dt>
											<dd><a href="http://www.vmall.com/product/2699.html" >荣耀 圈铁耳机
																							</a></dd>
											<dd><a href="http://www.vmall.com/product/2205.html#4519" >荣耀小口哨
																							</a></dd>
											<dd><a href="http://www.vmall.com/product/1337.html#2303,51" >耐翔 蜂巢 蓝牙耳机
																							</a></dd>
								</dl>
								</ul>
						</div>
				</li>
				<li class="category-item odd">
					<div class="category-info">
						<h3>
							<a href="/list-54" target="_blank"><span>通用配件
													</span></a></h3>
								<a href="/list-229" ><span>耳机
																</span></a>
								<a href="/list-55" target="_blank"><span>音箱
																</span></a>
								<a href="/list-56" target="_blank"><span>移动电源
																</span></a>
					</div>
					
						
						
								<div class="category-panels children-col-2">
												<ul class="subcate-list children-col-list">
											<li class="subcate-item">
												<a href="/list-229" >
													<span>耳机
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-55" target="_blank">
													<span>音箱
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-56" target="_blank">
													<span>移动电源
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-231" >
													<span>自拍杆
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-58" target="_blank">
													<span>充电器/线材
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-83" target="_blank">
													<span>U盘/存储卡
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-233" target="_blank">
													<span>支架
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-235" target="_blank">
													<span>摄像机/镜头
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-239" target="_blank">
													<span>智能硬件
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="/list-237" target="_blank">
													<span>车载电器
																											</span>
												</a>
											</li>
												</ul>
									<ul class="subcate-list children-col-list">
								<dl class="category-banner children-col-2">
									<dt><span>推荐商品</span></dt>
											<dd><a href="http://www.vmall.com/product/2134.html" >小天鹅蓝牙音箱
																							</a></dd>
								</dl>
								</ul>
						</div>
				</li>
				<li class="category-item ">
					<div class="category-info">
						<h3>
							<a href="http://app.vmall.com" target="_blank"><span>应用市场
													</span></a></h3>
								<a href="http://app.vmall.com/game/list" target="_blank"><span>手机游戏
																</span></a>
								<a href="http://app.vmall.com" target="_blank"><span>手机应用
																</span></a>
					</div>
					
						
						
								<div class="category-panels children-col-1">
												<ul class="subcate-list children-col-list">
											<li class="subcate-item">
												<a href="http://app.vmall.com/game/list" target="_blank">
													<span>手机游戏
																											</span>
												</a>
											</li>
											<li class="subcate-item">
												<a href="http://app.vmall.com" target="_blank">
													<span>手机应用
																											</span>
												</a>
											</li>
						</div>
				</li>
		</ol>
	</div>
</div>
<!-- 20140822-分类-end -->
		
		<!-- 20140823-分类-end -->
		<!-- 20130909-导航-start -->
		<nav class="naver">
				<ul id="naver-list">
			<li id="index"><a href="/index.html" 
               onclick="_paq.push(['trackLink','点击首页活动导航第1位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 

            class="current" 

            >
  
             <span>首 页
   
					</span></a> </li>
		<li id="huawei"><a href="http://www.vmall.com/huawei" 
               onclick="_paq.push(['trackLink','点击首页活动导航第2位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 

             

            target="_blank">
  
             <span>华为专区
   
					</span></a> </li>
		<li id="honor"><a href="http://www.vmall.com/honor" 
               onclick="_paq.push(['trackLink','点击首页活动导航第3位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 

             

            target="_blank">
  
             <span>荣耀专区
   
					</span></a> </li>
		<li id="honor"><a href="http://sale.vmall.com/ry8.html" 
               onclick="_paq.push(['trackLink','点击首页活动导航第4位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 

             

            target="_blank">
  
             <span>荣耀8
   
			<s><img src="/homePublic/images/new.png" alt="new" /></s>
		</span></a> </li>
		<li id="HUAWEI"><a href="http://www.vmall.com/product/3325.html" 
               onclick="_paq.push(['trackLink','点击首页活动导航第5位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 

             

            target="_blank">
  
             <span>P9
   
			<s><img src="/homePublic/images/hot.png" alt="hot" /></s>
					</span></a> </li>
		<li id="honor"><a href="http://www.vmall.com/product/186517099.html" 
               onclick="_paq.push(['trackLink','点击首页活动导航第6位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 

             

            target="_blank">
  
             <span>荣耀V8
   
			<s><img src="/homePublic/images/hot.png" alt="hot" /></s>
					</span></a> </li>
		<li id="peijian"><a href="http://sale.vmall.com/peijian.html" 
               onclick="_paq.push(['trackLink','点击首页活动导航第7位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 

             

            target="_blank">
  
             <span>配件专区
   
					</span></a> </li>
		<li><a href="javascript:;" ><span>精彩频道</span><i></i></a>
			<ol>
					<li id="honor">
                 <a href="http://www.vmall.com/honor/remind" 

                       onclick="_paq.push(['trackLink','点击首页活动导航第8位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 
                 
                  target="_blank">开售提醒</a></li>
					<li id="smrz">
                 <a href="http://www.vmall.com/notice-657" 

                       onclick="_paq.push(['trackLink','点击首页活动导航第9位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 
                 
                  target="_blank">实名认证</a></li>
					<li id="honor">
                 <a href="http://www.vmall.com/recycle?url=%3Fpid%3D1032" 

                       onclick="_paq.push(['trackLink','点击首页活动导航第10位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 
                 
                  target="_blank">以旧换新</a></li>
					<li id="hyj">
                 <a href="http://www.vmall.com/list-38" 

                       onclick="_paq.push(['trackLink','点击首页活动导航第11位', 'link', '']);ec.code.addAnalytics({hicloud:true});" 
                 
                  target="_blank">合约机</a></li>
			</ol>
		</li>
</ul>
<script>
	$(function () {
		$('#naver-list li').hover(function () {
			$(this).addClass('hover');
		},function () {
			$(this).removeClass('hover');
		});

		$('naver-list a').click(function(id){
            alert(id); 
        });
	});
</script>
			
		</nav><!-- 20130909-导航-end -->
		</div>
	</div>
</div>
<script>
(function () {
	//所有分类显示隐藏控制
	var isIndex =  false,
		categoryBlock = gid('category-block');
		
	if(isIndex) return;
	
	$("#category-block").hover(function(){
		$(this).addClass("category-hover");
	},function(){
		$(this).removeClass("category-hover");
	});
	
	/*categoryBlock.onmouseover = function () {
		categoryBlock.className = 'category category-hover';
	};
	categoryBlock.onmouseout = function () {
		categoryBlock.className = 'category';
	};*/
	
}());

/**
*功能：给鼠标移动到category-list li上面的时候，记得给他多添加一个hover类样式
*目的是兼容ie6,以及调整二级分类的弹出框的位置。
*@author 李峰
*/
/**
$(function(){
	$(".category-item").hover(function(){
		$(this).addClass("hover");
		//1.二级分类的top值
		var childrenTop = $(this).offset().top;
		//2.一级分类的top值
		var parentTop = $(".category-list").offset().top;
		//3.二级分类到一级分类顶部的距离
		var top = childrenTop - parentTop;
		//4.二级分类的弹出层的高度
		var childrenHeight = $(this).find(".category-panels").innerHeight();
		//5.一级分类容器的总高度
		var totalHeight = $(".category-list").height();
		alert("childrenTop:"+childrenTop+";parentTop:"+parentTop+";top:"+top+";childrenHeight:"+childrenHeight+";totalHeight:"+totalHeight) ;

		//6.如果二级分类.category-panels的内容高度大于总容量totalHeight,那么.category-panels置顶，然后多余的自动往下延续
		//如果二级分类childrenHeight内容高度(childrenHeight + top )大于totalHeight,那么.category-panels往上移动childrenHeight + top -totalHeight
		if((top + childrenHeight) > totalHeight)
		{
			if(childrenHeight > totalHeight)
			{
				$(this).find(".category-panels").css("top",-top);
			}else{
				//上移动
				var topX = (childrenHeight+top) - totalHeight;
				$(this).find(".category-panels").css("top",-topX);
				
			}
			
		}
		
	},function(){
		$(this).removeClass("hover");
	});
});
 */
</script>
<script>
$("body").addClass("wide detail");
</script>

@section('content')

@show

<!--口号-20121025 -->
<div class="slogan">
	<ul>
		<li class="s1"><i></i>500强企业&nbsp;品质保证</li>
		<li class="s2"><i></i>7天退货&nbsp;15天换货</li>
		<!-- <li class="s3"><i></i>99元起免运费</li> -->
		<li class="s3"><i></i>
			<span>99元起免运费</span>
</li>
		<li class="s4"><i></i>448家维修网点&nbsp;全国联保</li>
	</ul>
</div><!--口号-end -->
<!--服务-20121025 -->
<div class="service">
		<dl class="s1">
			<dt><i></i>帮助中心</dt>
			<dd>
				<ol>
					<li><a target="_blank" href="http://www.vmall.com/help/faq-790.html">购物指南</a></li>
					<li><a target="_blank" href="http://www.vmall.com/help/faq-814.html">配送方式</a></li>
					<li><a target="_blank" href="http://www.vmall.com/help/faq-828.html">支付方式</a></li>
					<li><a target="_blank" href="http://www.vmall.com/help/category-16.html">常见问题</a></li>
				</ol>
			</dd>
		</dl>
		<dl class="s2">
			<dt><i></i>售后服务</dt>
			<dd>
				<ol>
					<li><a target="_blank" href="http://www.vmall.com/help/faq-833.html">保修政策</a></li>
					<li><a target="_blank" href="http://www.vmall.com/help/faq-834.html">退换货政策</a></li>
					<li><a target="_blank" href="http://www.vmall.com/help/faq-835.html">退换货流程</a></li>
					<li><a target="_blank" href="http://consumer.huawei.com/cn/support/warranty-query/index.htm">保修状态查询</a></li>
				</ol>
			</dd>
		</dl>
		<dl class="s3">
			<dt><i></i>技术支持</dt>
			<dd>
				<ol>
					<li><a target="_blank" href="http://consumer.huawei.com/cn/support/service-center/index.htm">售后网点</a></li>
					<li><a target="_blank" href="http://consumer.huawei.com/cn/support/reservation/index.htm">预约维修</a></li>
					<li><a target="_blank" href="http://consumer.huawei.com/cn/support/express-repair/index.htm">手机寄修服务</a></li>
					<li><a target="_blank" href="http://consumer.huawei.com/cn/support/sparepart-price/index.htm">维修配件价格查询</a></li>
					<li><a target="_blank" href="http://app.hicloud.com/">软件下载</a></li>
				</ol>
			</dd>
		</dl>
		<dl class="s4">
			<dt><i></i>关于商城</dt>
			<dd>
				<ol>
					<li><a target="_blank" href="http://www.vmall.com/help/faq-934.html">公司介绍</a></li>
					<li><a target="_blank" href="http://www.vmall.com/help/faq-939.html">华为商城简介</a></li>
					<li><a target="_blank" href="http://www.vmall.com/help/index.html">联系客服</a></li>
				</ol>
			</dd>
		</dl>
		<dl class="s5">
			<dt><i></i>关注我们</dt>
			<dd>
				<ol>
					<li><a class="sina" rel="nofollow" href="http://weibo.com/shophuawei" target="_blank">新浪微博</a></li>
					<li><a class="qq" rel="nofollow" href="http://t.qq.com/shophuawei" target="_blank">腾讯微博</a></li>
					<li><a class="huafen" href="http://club.huawei.com" target="_blank">花粉社区</a></li>
					<li><a href="http://www.vmall.com/appdownload" target="_blank">商城手机版</a></li>
				</ol>
			</dd>
		</dl>
</div>
<!--服务-end -->

<!--确认对话框-->
<div id="ec_ui_confirm" class="popup-area popup-define-area hide">
    <div class="b">
        <p id="ec_ui_confirm_msg"></p>
        <div class="popup-button-area"><a id="ec_ui_confirm_yes" href="javascript:;" class="button-action-yes" title="是"><span>是</span></a><a id="ec_ui_confirm_no" href="javascript:;" class="button-action-no" title="否"><span>否</span></a></div>
    </div>
    <div class="f"><s class="icon-arrow-down"></s></div>
</div>

<!--新确认对话框-->
<div id="ec_ui_confirm_new" class="popup-area-new hide">
	<div class="b">
		<p id="ec_ui_confirm_new_msg"></p>
		<div class="popup-button-area"><a id="ec_ui_confirm_new_yes" href="javascript:;" class="box-button-style-1" title="是"><span>是</span></a><a id="ec_ui_confirm_new_no" href="javascript:;" class="box-button-style-1" title="否"><span>否</span></a></div>
	</div>
	<div class="f"><s class="icon-arrow-down-new"></s></div>
</div>

<!--提示对话框-->
<div id="ec_ui_tips" class="popup-area popup-define-area hide">
    <div class="b">
        <p id="ec_ui_tips_msg" class="tac"></p>
        <div class="popup-button-area tac"><a id="ec_ui_tips_yes" href="javascript:;" class="button-action-yes" title="确定"><span>确定</span></a></div>
    </div>
    <div class="f"><s class="icon-arrow-down"></s></div>
</div>

<!--在线客服-->
<div class="hungBar" id="tools-nav">
	<a title="返回顶部" class="hungBar-top" href="#" id="hungBar-top">返回顶部</a>
	<a id="tools-nav-survery" title="意见反馈" class="hungBar-feedback hide" href="javascript:;" onclick="ec.survery.open();">意见反馈</a>		
	<a id="tools-nav-service-qq" title="QQ客服" class="hungBar-olcs-qq hide" href="http://wpa.qq.com/msgrd?V=1&Uin=4000886888" target="_blank">QQ客服</a>
	<a id="tools-nav-service-robotim" href="javascript:;" title="在线客服" class="hungBar-olcs-web hide" target="_blank">在线客服</a>

	<!--意见反馈box-->
	<div id="survery-box" class="form-feedback-area hide">
		<div class="h">
			<a class="form-feedback-close" title="关闭" onclick="ec.survery.close();return false;" href="javascript:;"></a>
		</div>
		<div class="b">
				<div class="form-edit-area">
					<form onsubmit="return ec.survery.submit(this);" autocomplete="off">
						<div class="form-edit-table">
							<table cellspacing="0" cellpadding="0" border="0">
								<tbody>
							    <tr>
                                    <td>
                                        <b>疑问类型：</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="type" name="type">
                                        	<option>请选择疑问类型</option>
                                            <option>商品质量（手机、平板等软、硬件质量）</option>
                                            <option>商品缺货</option>
                                            <option>物流发货（发货快慢、发错货、送件人态度等）</option>
                                            <option>售后服务（服务网点、维修、退换货、客服）</option>
                                            <option>网站问题（商城功能失效、不好用等）</option>    
                                            <option>其他问题</option>    
                                        </select>
                                    </td>
                                </tr>
								
								<tr>
									<td><b>您的问题或建议：</b><span id="errMsg"></span></td>
								</tr>
								<tr>
									<td><textarea class="textarea" type="textarea" name="content" id="surveryContent"></textarea></td>
								</tr>
								<tr>
									<td>您的联系方式：</td>
								</tr>
								<tr>
									<td><input type="text" class="text" name="contact" id="surveryContact"></td>
								</tr>
								<tr>
									<td><div class="fl"><input type="text" name="code" id="surveryVerify" class="verify vam" maxlength="4">&nbsp;&nbsp;<img id="surveryVerifyImg" onclick="ec.survery.reloadCode()" class="vam" alt="验证码">&nbsp;&nbsp;&nbsp;&nbsp;<span class="vam"><a onclick="ec.survery.reloadCode();" href="javascript:;" class="u">看不清，换一张</a></span></div><div class="fr"><input type="submit" value="" class="button-action-submit-3"></div></td>
								</tr>
								</tbody>
							</table>
						</div>
					</form>
				</div>
		</div>
	</div>
	
</div>
<div id="globleParameter" class="hide" context="" stylePath="http://res8.vmallres.com/20160712/css" scriptPath="http://res9.vmallres.com/20160712/js" imagePath="http://res.vmallres.com/20160712/images" mediaPath="http://res.vmallres.com/pimages/" ></div>
<!--底部 -->

<footer class="footer">
    <!-- 20130902-底部-友情链接-start -->
	<div class="footer-otherLink">
		<p style="text-align:center;">热门<a href="http://www.vmall.com/list-36" target="_blank">华为手机</a>：<a href="http://www.vmall.com/product/186517099.html" target="_blank">荣耀V8</a> |<span style="line-height:1.5;"> <a href="http://www.vmall.com/product/3325.html" target="_blank">华为P9</a> | <a href="http://www.vmall.com/product/2647.html" target="_blank">华为Mate8</a> | <a href="http://www.vmall.com/product/2172.html" target="_blank">荣耀7</a> | <a href="http://www.vmall.com/product/3451.html" target="_blank">荣耀5C</a> | <a href="http://www.vmall.com/product/1896.html" target="_blank">华为P8</a> | <a href="http://www.vmall.com/product/2324.html" target="_blank">荣耀7i</a> | <a href="http://www.vmall.com/product/2519.html" target="_blank">荣耀畅玩5X</a> | </span><a href="http://www.vmall.com/product/641202729.html" target="_blank" textvalue="荣耀5A"><span style="line-height:1.5;">荣耀5A</span></a><span style="line-height:1.5;"> | </span><span style="line-height:1.5;"><a href="http://www.vmall.com/recycle" target="_blank">以旧换新</a> </span></p>
	</div>
	<div class="footer-warrant-area"><p><a textvalue="隐私政策" title="隐私政策" target="_blank" href="http://www.vmall.com/help/faq-2635.html">隐私政策</a>  <a title="服务协议" target="_blank" href="http://www.vmall.com/help/faq-778.html">服务协议</a>        Copyright © 2012-2016  VMALL.COM   版权所有  保留一切权利</p><p><a target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=32011402010009"><img style="width:20px;height:20px;float:none;" src="images/20160226162651249.png" title="公安备案" border="0" height="20" hspace="0" vspace="0" width="20" /><span style="font-size:15px;font-family:宋体"><span style="font-size:12px;font-family:arial,helvetica,sans-serif;"> 苏公网安备</span><span style="font-size:12px;font-family:arial,helvetica,sans-serif;"> 32011402010009号</span></span></a> | 苏ICP备09062682号-9 | 增值电信业务经营许可证：苏B2-20130048 | <a target="_blank" href="http://res.vmallres.com/certificate/wwwz.jpg">网络文化经营许可证：苏网文[2012]0401-019号</a></p><p><br /><a target="_blank" href="http://res.vmallres.com/certificate/wwwz.jpg"></a></p><p><a target="_blank" href="http://www.jsgsj.gov.cn:60103/businessCheck/verifKey.do?serial=320100913201147770231720001001-SAIC_SHOW_32000020160129102316602&amp;signData=MEUCIQDjstcg6fPylz+uES4LNTcBpBVlZujS8l5erIgXiDGw1QIgfHZFK11kZ1vB2enLCwvaslyfE1fztpB19AEK4hlwibo="><img src="/homePublic/images/20160226162415360.png" style="float:none;width:90px;height:32px;" title="电子营业执照" border="0" height="32" hspace="0" vspace="0" width="90" /></a>   <a target="_blank" href="https://ss.knet.cn/verifyseal.dll?sn=e13010932010038497pwz6000000&amp;trustKey=dn&amp;trustValue=vmall.com"><img style="width:90px;height:32px;float:none;" src="images/20160226162521265.png" title="可信网站" border="0" height="32" hspace="0" vspace="0" width="90" /></a>   <a target="_blank" href="https://search.szfw.org/cert/l/CX20121017001773002082"><img style="width:90px;height:32px;float:none;" src="images/20160226162531395.png" title="诚信网站" border="0" height="32" hspace="0" vspace="0" width="90" /></a></p></div><!--授权结束 --><script>
//客服工具
//ec.code.addService({showService : true , showTools : true});
ec.code.addAnalytics({
  baidu : true,
  google : false,
  hicloud : true,
  cnzz : false,
  suning : false,
  dmp:true
});</script>
</footer>


</body>
</html>